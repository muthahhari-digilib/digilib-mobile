import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'history.dart';
import 'main_page.dart';
import 'backend_conn.dart';

class SearchPage extends StatefulWidget {
  final BackendConnection backendConnection;

  const SearchPage({Key? key, required this.backendConnection})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => SearchPageState();
}

class SearchPageState extends State<SearchPage> {
  final List<String> types = [];
  Future<DigilibSearchResult>? _search;
  var _types;
  final query = TextEditingController();
  SearchHistory? searchFile;
  bool searchHistoryShown = true;

  @override
  void initState() {
    super.initState();

    _types = widget.backendConnection.getTypes();
    _search = (() async {
      // return empty result for starting point
      return DigilibSearchResult(0, 1, []);
    })();
  }

  Future<DigilibSearchResult> _doSearch() async {
    var f0 = addSearchHistory(query.text);
    var f1 = widget.backendConnection
        .search(query.text, DigilibSearchOptions(type: types));
    var res = (await Future.wait<dynamic>([f1, f0]))[0];

    return res;
  }

  void search() {
    if (query.text.trim() == '')
      return;

    setState(() {
      _search = _doSearch();
      searchHistoryShown = false;
    });
    FocusScope.of(context).requestFocus(FocusNode());
  }

  Future<List<String>> getSearchHistory() async {
    await _initSearchHistory();
    return (await searchFile!.getSearchHistory())
        .where((element) =>
            element.toLowerCase().contains(query.text.toLowerCase()))
        .toList();
  }

  Future _initSearchHistory() async {
    if (searchFile == null) {
      searchFile = await SearchHistory.standard();
    }
  }

  Future<String> addSearchHistory(String query) async {
    await _initSearchHistory();
    searchFile!.addHistory(query);
    return query;
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
          title: Container(
            child: TextField(
              autofocus: true,
              decoration: InputDecoration(
                hintText: 'Cari...',
              ),
              controller: query,
              onChanged: (value) {
                setState(() {
                  searchHistoryShown = true;
                });
              },
              onEditingComplete: search,
              textInputAction: TextInputAction.search,
            ),
            decoration: BoxDecoration(color: Colors.white38),
            padding: EdgeInsets.only(left:5),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(media.orientation == Orientation.portrait ? 60 : 30),
            child: FutureBuilder<List<String>>(
              future: _types,
              builder: (context, future) {
                if (!future.hasData) return CircularProgressIndicator();
                return Padding(padding: EdgeInsets.all(7), child: Wrap(
                  spacing: 5,
                  runSpacing: 5,
                  alignment: WrapAlignment.center,
                  children: future.data!
                      .map((e) {
                        var selected = types.contains(e);
                        return InkWell(
                          onTap: () =>
                            setState(() {
                              if (selected)
                                types.remove(e);
                              else
                                types.add(e);
                              if (!searchHistoryShown)
                                search();
                            }),
                          child: Container(
                              padding: EdgeInsets.symmetric(horizontal:7, vertical: 3),
                              child: Text(e),
                              decoration: BoxDecoration(
                                color: selected ? Colors.amber : Colors.white,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: kElevationToShadow[selected ? 12 : 5]),));
                        }
                      ).toList()
                    //isSelected: future.data!.map((e) => true).toList()
                    ));
              },
            ),
          )),
      body: FutureBuilder<DigilibSearchResult>(
        key: Key('search$query'),
        future: _search,
        builder: (context, future) {
          if (searchHistoryShown)
            return FutureBuilder<List<String>>(
                future: getSearchHistory(),
                builder: (context2, future2) {
                  if (future2.hasError)
                    return Center(child: Text('Error: ${future2.error}'));
                  if (!future2.hasData)
                    return Center(child: CircularProgressIndicator());

                  return ListView.builder(
                    itemCount: future2.data?.length ?? 0,
                    itemExtent: 50,
                    itemBuilder: (context2, i) => InkWell(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                              padding: EdgeInsets.all(5),
                              child: FaIcon(FontAwesomeIcons.history)),
                          Expanded(
                              child: Text(
                            future2.data![future2.data!.length - i - 1],
                            maxLines: 2,
                          )),
                          Padding(
                              padding: EdgeInsets.all(5),
                              child: FaIcon(FontAwesomeIcons.longArrowAltUp))
                        ],
                      ),
                      onTap: () {
                        query.text =
                            future2.data![future2.data!.length - i - 1];
                        search();
                      },
                    ),
                  );
                });
          const baseWidth = 360.0;
          final width = MediaQuery.of(context).size.width;
          final factor = media.orientation == Orientation.portrait ? (width / baseWidth) : 1;

          return Container(
            child: 
              (future.connectionState == ConnectionState.active || future.connectionState == ConnectionState.waiting)
              ? Center(child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(Colors.white)))
              : (future.hasError)
                ? SingleChildScrollView(
                    child: Column(children: [
                    Center(
                        child: Container(
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(color: Colors.white70),
                            child: Text('Error: ${future.error}')))
                  ]))
                : ListView.builder(
                        itemCount: future.data!.contents.length,
                        itemExtent: factor * 96 + 4 + 14,
                        itemBuilder: (context, i) => ContentWidget(
                            backendConnection: widget.backendConnection,
                            content: future.data!.contents[i]),
                      ),
          );
        },
      ),
    );
  }
}
