import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:intl/intl.dart';
import 'package:pointycastle/export.dart';

class KeyPair {
  final String publicKey;
  final String privateKey;
  KeyPair({required this.publicKey, required this.privateKey});
}

class DigilibCrypto {
  var decryptor;
  var bookReturnDateStr;

  DigilibCrypto(String mobilePhonePrivateKey, DateTime bookReturnDate) {
    final priv = jsonDecode(mobilePhonePrivateKey);
    final myPrivate = RSAPrivateKey(
      BigInt.parse(priv['modulo']),
      BigInt.parse(priv['exponent']),
      BigInt.parse(priv['p']),
      BigInt.parse(priv['q'])
    );
    decryptor = OAEPEncoding(RSAEngine())
      ..init(false, PrivateKeyParameter<RSAPrivateKey>(myPrivate));
    
    bookReturnDateStr = new DateFormat('yyyy-MM-dd').format(bookReturnDate);
  }
  static KeyPair generateMobilePhoneKeys() {
    final secureRandom = FortunaRandom();
    final seedSource = Random.secure();
    final seeds = <int>[];
    for (int i = 0; i < 32; i++) {
      seeds.add(seedSource.nextInt(255));
    }
    secureRandom.seed(KeyParameter(Uint8List.fromList(seeds)));
    
    final keyGen = RSAKeyGenerator()
      ..init(ParametersWithRandom(RSAKeyGeneratorParameters(BigInt.parse('65537'), 2048, 64), secureRandom));
    
    final pair = keyGen.generateKeyPair();
    
    final myPublic = pair.publicKey as RSAPublicKey;
    final myPrivate = pair.privateKey as RSAPrivateKey;

    return KeyPair(
      publicKey: jsonEncode({
        'exponent': myPublic.publicExponent.toString(),
        'modulo': myPublic.modulus.toString()
      }),
      privateKey: jsonEncode({
        'exponent': myPrivate.privateExponent.toString(),
        'modulo': myPrivate.modulus.toString(),
        'p': myPrivate.p.toString(),
        'q': myPrivate.q.toString()
      }));
  }

  String _decryptKey(String encryptedKey) {
    final ciphertext = base64Decode(encryptedKey);
    final plaintext = _processInBlocks(decryptor, ciphertext);
    return Utf8Decoder().convert(plaintext);
  }

  Uint8List _processInBlocks(AsymmetricBlockCipher engine, Uint8List input) {
    final numBlocks = input.length ~/ engine.inputBlockSize +
        ((input.length % engine.inputBlockSize != 0) ? 1 : 0);

    final output = Uint8List(numBlocks * engine.outputBlockSize);

    var inputOffset = 0;
    var outputOffset = 0;
    while (inputOffset < input.length) {
      final chunkSize = (inputOffset + engine.inputBlockSize <= input.length)
          ? engine.inputBlockSize
          : input.length - inputOffset;

      outputOffset += engine.processBlock(
          input, inputOffset, chunkSize, output, outputOffset);

      inputOffset += chunkSize;
    }

    return (output.length == outputOffset)
        ? output
        : output.sublist(0, outputOffset);
  }

  Uint8List _processStream(StreamCipher engine, Uint8List input) {
    final output = Uint8List(input.length);

    engine.processBytes(input, 0, input.length, output, 0);

    return output;
  }

  Uint8List decryptSvg(Uint8List ciphertext, String encryptedKey) {
    final Map<String, dynamic> key = jsonDecode(this._decryptKey(encryptedKey));
    final finalKey = Utf8Encoder().convert(key['key'] + bookReturnDateStr);
    final nonce = base64Decode(key['nonce']);

    final cipher = ChaCha20Engine()
      ..init(false, ParametersWithIV(KeyParameter(finalKey), nonce));

    return this._processStream(cipher, ciphertext);
  }
}