import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import 'main.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
        title: Text(''),
        
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: DigilibColors.secondary),
    ),
    extendBodyBehindAppBar: true,
    body: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/appbar_bg.png'),
              alignment: Alignment.topCenter,
              fit: BoxFit.cover
            ),
            color: DigilibColors.primaryAlternative,
          ),
          padding: EdgeInsets.fromLTRB(19, 84, 19, 14),
          child: Text('Tentang Aplikasi', style: Theme.of(context).primaryTextTheme.headline6)
        ),
        Expanded(child: SingleChildScrollView(child: Padding(
          padding: EdgeInsets.fromLTRB(19, 14, 19, 14),
          child: Text.rich(TextSpan(children: [
            TextSpan(text: """Aplikasi untuk koleksi karya dan pemikiran Allahyarham KH. Jalaluddin Rakhmat beserta penulis dan tokoh lain yang menelaah, menyebarkan dan meneruskan karya Kang Jalal. Nama aplikasi "Jalan Rahmat" diambil dari salah satu karya beliau yang berbentuk buku.

Perpustakaan ini mencakup topik-topik yang menjadi perhatian Allahyarham, yaitu:

• Ahlul Bait
• Tafsir Al-Qur'an
• Hadits
• Filsafat
• Sejarah
• Psikologi
• Sains dan Pendidikan
• Komunikasi
• Agama
• Fikih
• Sosial Politik
• Tasawuf
• Neurosains
• Do'a""")
          ]), textAlign: TextAlign.justify,)
        ))),
        Padding(
          padding: EdgeInsets.all(7),
          child: FutureBuilder(
            future: (() async {
              final packageInfo = await PackageInfo.fromPlatform();
              return '${packageInfo.appName} ${packageInfo.packageName}\n' +
              'v${packageInfo.version}+${packageInfo.buildNumber} sig:${packageInfo.buildSignature}';
            })(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                return LinearProgressIndicator();
              }
              
              return Text('${snapshot.data}',
                textAlign: TextAlign.right,
                textScaleFactor: 0.7
              );
            },
          )
        )
      ])
  );
}

class FeedbackPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
        title: Text(''),
        
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: DigilibColors.secondary),
    ),
    extendBodyBehindAppBar: true,
    body: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/appbar_bg.png'),
              alignment: Alignment.topCenter,
              fit: BoxFit.cover
            ),
            color: DigilibColors.primaryAlternative,
          ),
          padding: EdgeInsets.fromLTRB(19, 84, 19, 14),
          child: Text('Umpan Balik', style: Theme.of(context).primaryTextTheme.headline6)
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(19, 14, 19, 14),
          child: Text.rich(TextSpan(children: [
            TextSpan(text: """Jangan ragu untuk menghubungi kami untuk pertanyaan Anda. Hubungi kami di kontak di bawah ini.""")
          ]), textAlign: TextAlign.justify,)
        ),
        InkWell(
          onTap: () async {
            final packageInfo = await PackageInfo.fromPlatform();
            final version = 'v${packageInfo.version}+${packageInfo.buildNumber} sig:${packageInfo.buildSignature}';
            
            final Uri params = Uri(
              scheme: 'mailto',
              path: 'info@jalanrahmat.id',
              query: 'subject=Umpan Balik JR Digilib App&body=JR Digilib App Version $version',
            );

            var url = params.toString();
            if (await canLaunch(url)) {
              await launch(url);
            } else {
              showDialog(context: context, builder: (context) =>
                AlertDialog(
                  content: Text('Tidak dapat mengirim email'),
                  actions: [
                    TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
                  ],
                )
              );
            }
          },
          child: Padding(
            padding: EdgeInsets.fromLTRB(19, 14, 19, 14),
            child: Row(children: [
              Icon(Icons.email_outlined, size: 36),
              SizedBox(width: 14),
              Column(children: [
                Text('Email'),
                Text('info@jalanrahmat.id', style: TextStyle(color: DigilibColors.secondary)),
              ], crossAxisAlignment: CrossAxisAlignment.start)
            ]))),
        Spacer(),
        Padding(
          padding: EdgeInsets.all(7),
          child: FutureBuilder(
            future: (() async {
              final packageInfo = await PackageInfo.fromPlatform();
              return '${packageInfo.appName} ${packageInfo.packageName}\n' +
              'v${packageInfo.version}+${packageInfo.buildNumber} sig:${packageInfo.buildSignature}';
            })(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                return LinearProgressIndicator();
              }
              
              return Text('${snapshot.data}',
                textAlign: TextAlign.right,
                textScaleFactor: 0.7
              );
            },
          )
        )
      ])
  );
}