import 'package:digilib/notification_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'main.dart';

class NotificationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
        title: Text(''),
        
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: DigilibColors.secondary),
    ),
    extendBodyBehindAppBar: true,
    body: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/appbar_bg.png'),
              alignment: Alignment.topCenter,
              fit: BoxFit.cover
            ),
            color: DigilibColors.primaryAlternative,
          ),
          padding: EdgeInsets.fromLTRB(19, 84, 19, 14),
          child: Text('Notifikasi', style: Theme.of(context).primaryTextTheme.headline6)
        ),
        Row(children: [
          Spacer(),
          TextButton(
            onPressed: () => NotificationStore().clear(),
            child: Text('Hapus Semua')
          ),
        ]),
        FutureBuilder<List<NotificationData>>(
          future: NotificationStore().getData(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Expanded(child: Center(child: CircularProgressIndicator()));
            }
            if ((snapshot.data?.length ?? 0) == 0) {
              return Center(child: Text('Belum ada notifikasi'));
            }
            return Expanded(child: ListView.separated(
              padding: EdgeInsets.all(14),
              itemCount: snapshot.data?.length ?? 0,
              itemBuilder: (context, i) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(DateFormat().format(snapshot.data?[i].time ?? DateTime.now()),
                    style: Theme.of(context).textTheme.subtitle2),
                  SizedBox(height: 5),
                  Text(snapshot.data?[i].text ?? 'Notification received')
                ],
              ),
              separatorBuilder: (context, i) => Divider(),
            ));
          }
        )
      ])
  );
}