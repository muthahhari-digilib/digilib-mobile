import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'digilibcrypto.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:synchronized/synchronized.dart';
import 'package:carousel_slider/carousel_slider.dart';

class HidingAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar child;
  final Animation animation;

  HidingAppBar({required this.animation, required this.child});

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (context, c) => Transform.translate(
        offset: Offset(0, -animation.value * 2 * child.preferredSize.height),
        child: child,
      ));
  }

  @override
  Size get preferredSize => child.preferredSize;
}

class DigilibBookPage extends StatefulWidget {
  DigilibBookPage({Key? key, required this.bookZipFile, required this.bookTitle, required this.bookKeys, required this.mobilePhonePrivateKey, required this.returnDate, required this.bookLen}) : super(key: key);

  final File bookZipFile;
  final String bookTitle;
  final int bookLen;
  final List<String> bookKeys;
  final String mobilePhonePrivateKey;
  final DateTime returnDate;

  @override
  _DigilibBookPageState createState() => _DigilibBookPageState();

}

class _DigilibBookPageState extends State<DigilibBookPage> 
    with TickerProviderStateMixin {
  bool loading = true;
  late AnimationController _controller;
  CarouselController _carouselController = CarouselController();
  int _carouselCurrentPage = 0;
  int _sliderValue = 1;

  Widget? currentPage;
  TransformationController tc = TransformationController();
  late AnimationController _zoomController;

  _DigilibBookPageState() {
    _controller = AnimationController(
      vsync: this, duration: Duration(milliseconds: 300),
      value: 1
    );
    _zoomController = AnimationController(
      vsync: this, duration: Duration(milliseconds: 300)
    );
  }

  initState() {
    super.initState();
  }
  bool get fullscreen {
    return _controller.isCompleted;
  }
  set fullscreen(bool value) {
    if (value)
      _controller.forward();
    else {
      setState(() {
        currentPage = null;
      });
      _controller.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return WillPopScope(
    onWillPop: () async {
        if (currentPage != null) {
          setState(() {
            currentPage = null;
          });
          return false;
        }
        if (fullscreen) {
          fullscreen = false;
          return false;
        }
        return true;
      },
    child: Scaffold(
      appBar: HidingAppBar(
        animation: _controller,
        child: AppBar(
          title:Text(widget.bookTitle),
          actions: [
            IconButton(
              icon: Icon(Icons.zoom_in),
              onPressed: () async {
                var cp = await _book?.getPage(_carouselCurrentPage);
                if (cp == null) return;
                setState(() => currentPage = cp);
                fullscreen = true;
                tc.value = Matrix4.identity();
                _zoomController.reset();
                _zoomController.forward();
              },
            )
          ],
        )),
      extendBodyBehindAppBar: true,
      extendBody: true,
      backgroundColor: Colors.brown.shade100,
      bottomNavigationBar: AnimatedBuilder(
        animation: _controller,
        builder: (ctx, child) => Transform.translate(
            offset: Offset(0, 70 * _controller.value),
            child: child,
          ),
        child: Container(
          height: 70,
          decoration: BoxDecoration(color: Colors.brown.shade50),
          child: Column(
            children: [
              Slider.adaptive(
                min: 1.0,
                max: widget.bookLen.toDouble(),
                divisions: widget.bookLen,
                value: _sliderValue.toDouble(),
                onChangeEnd: (value) => _carouselController.jumpToPage(value.toInt() - 1),
                onChanged: (value) => setState(() => _sliderValue = value.toInt()),
                label: 'Halaman $_sliderValue/${widget.bookLen}',),
              Text('Halaman $_sliderValue/${widget.bookLen}')
            ]))),

      body: FutureBuilder<DigilibBook>(
          future: readBook(),
          builder: (context, snapshot) {
            //var width = MediaQuery.of(context).size.width;

            //var padding = MediaQuery.of(context).padding;
            //var newwidth = width - padding.left - padding.right;

            if (snapshot.hasData) {
              var book = snapshot.data;
              if (book == null)
                return Center(child: Text('Invalid book: null'));
              
              if (currentPage != null)
                return Container(
                  decoration: BoxDecoration(color: Colors.black),
                  width: double.infinity,
                  height: double.infinity,
                    child: GestureDetector(
                      onDoubleTap: () =>
                        setState(() => currentPage = null),
                      onTap: () =>
                        setState(() => currentPage = null),
                    child: AnimatedBuilder(
                      animation: _zoomController,
                      builder: (ctx, c) {
                        var mq = MediaQuery.of(ctx);
                        if (_zoomController.isAnimating) {
                          var m = Matrix4.identity();
                          m.scale(1 + 0.2 * _zoomController.value);
                          m.translate(
                            -0.1 * mq.size.width * _zoomController.value,
                            -0.1 * mq.size.height * _zoomController.value);
                          tc.value = m;
                        }
                        return InteractiveViewer(
                          transformationController: tc,
                          constrained: true,
                          onInteractionUpdate: (d) {
                            if (tc.value.getMaxScaleOnAxis() == 1) {
                              setState(() => currentPage = null);
                            }
                          },
                          child:Center(
                            child: Container(
                              decoration: BoxDecoration(color: Colors.white),
                              child: currentPage)));
                      })));

              //var dim = book.getDimension(0);
              return CarouselSlider.builder(
                carouselController: _carouselController,
                itemCount: book.pageCount,
                itemBuilder: (context, itemIndex, pageViewIndex) => bookPageBuilder(book, itemIndex),
                options: CarouselOptions(
                  enableInfiniteScroll: false,
                  disableCenter: true,
                  height: double.infinity,
                  viewportFraction: 1.0,
                  initialPage: _carouselCurrentPage,
                  onPageChanged: (i, reason) => setState(() {
                    _carouselCurrentPage=i;
                    _sliderValue=i+1;
                  })
                ),
              );
            } else {
              return CircularProgressIndicator();
            }
        }),
    ));
  }

  bool _first = true;
  Widget bookPageBuilder(DigilibBook book, int pageNo) {
    var width = MediaQuery.of(context).size.width;
    var padding = MediaQuery.of(context).padding;
    var newwidth = width - padding.left - padding.right;
    return FutureBuilder<Widget>(
      future: (() async {
        try {
          return await book.getPage(pageNo);
        } catch (e, stack) {
          print(e);
          print(stack);
          rethrow;
        } finally {
          if (_first) {
            _first = false;
            fullscreen = false;
          }
        }
      })(),
      builder: (context1, snapshot1) {
        if (!snapshot1.hasData && !snapshot1.hasError) {
          return Center(child: CircularProgressIndicator());
        }

        if (snapshot1.hasError) {
          return Center(child: Text('Error: ' + (snapshot1.error?.toString() ?? '')));
        }
        // FIXME: dari svg dijadikan pictnya di sini saja, supaya bisa menyesuaikan
        // kebutuhan resolusinya
        var pict = snapshot1.data!;

        final dim = book.getDimension(pageNo);
        return Center(
          child: GestureDetector(
            onTap: () => setState(() {
              fullscreen = !fullscreen;
            }),
            onDoubleTap: () => setState(() {
              fullscreen = true;
              currentPage = pict;
              tc.value = Matrix4.identity();
              _zoomController.reset();
              _zoomController.forward();
            }),
            onScaleStart: (d) => setState(() {
              fullscreen = true;
              currentPage = pict;
              tc.value = Matrix4.identity();
            }),
            child: Container(
                decoration: BoxDecoration(color: Colors.white),
                child: pict)));
      },

    );
  }

  DigilibBook? _book;
  Future<DigilibBook> readBook() async {
    if (_book != null)
      return _book!;
    var zipname = path.basenameWithoutExtension(widget.bookZipFile.path);
    var tempDir = await getTemporaryDirectory();
    var outDir = Directory(path.join(tempDir.path, zipname + '_'));
    if (!await outDir.exists()) {
      await outDir.create(recursive:true);
      final d = ZipDecoder().decodeBytes(await widget.bookZipFile.readAsBytes());

      for (final file in d) {
        final data = file.content as List<int>;
        
        final f = File(path.join(outDir.path, file.name));
        await f.create();
        await f.writeAsBytes(data);
      }
    }
    _book = DigilibBook(outDir, widget.bookKeys, widget.returnDate, widget.mobilePhonePrivateKey, cachePictures: true);
    return _book!;
  }
}

class DigilibBook {
  Directory dir;
  List<String> keys;

  int pageCount;
  bool hasInfo;
  NumberFormat numberFormat;

  DigilibCrypto crypto;
  List<BookPageDimension>? dimensions;
  Lock lock = Lock();
  late List<Widget?> pictures;
  bool cachePictures;

  DigilibBook (this.dir, this.keys, bookReturnDate, mobilePhonePrivateKey, {this.cachePictures = false}) :
      hasInfo = File(path.join(dir.path, 'info.json')).existsSync(),
      crypto = DigilibCrypto(mobilePhonePrivateKey, bookReturnDate),
      numberFormat = NumberFormat('000'),
      pageCount = dir.listSync().length {
    if (hasInfo) {
      pageCount --;
    }
    pictures = List.filled(cachePictures ? pageCount : 0, null);
  }

  /// pageNo starts from 0
  Future<Widget> getPage(int pageNo, {bool prefetchNext = true}) async {
    if (cachePictures && pictures[pageNo] != null) {
      // prefetch next page
      if (prefetchNext && pageNo < pageCount - 1)
        getPage(pageNo + 1, prefetchNext: false);
      return pictures[pageNo]!;
    }
    
    var fileNo = numberFormat.format(pageNo);
    var f = File(path.join(dir.path, fileNo + '.svg.enc'));
    var ret;
    if (f.existsSync()) {
      final svgCipher = await f.readAsBytes();
      await lock.synchronized(() async {
        //ret = SvgPicture.string(await compute(decrypt, DecryptData(crypto, svgCipher, keys[pageNo])));
        var pict = await svg.fromSvgString(await compute(decrypt, DecryptData(crypto, svgCipher, keys[pageNo])), "svg");
        var dim = getDimension(pageNo);
        var factor = 4;
        print('w: ${dim.width * factor} h: ${dim.height * factor}');
        if (dim.width < 0 || dim.height < 0) {
          ret = RawImage(image: await pict.toPicture(size: Size(720, 1280)).toImage(720, 1280));
        } else {
          ret = RawImage(image: await pict.toPicture(size: Size(dim.width * factor * 1, dim.height * factor * 1)).toImage(dim.width * factor, dim.height * factor));
        }
      });
    } else {
      f = File(path.join(dir.path, fileNo + '.svg'));
      ret = SvgPicture.string(await f.readAsString());
    }
    if (cachePictures) {
      pictures[pageNo] = ret;
      // prefetch next page
      if (prefetchNext && pageNo < pageCount - 1)
        getPage(pageNo + 1, prefetchNext: false);
    }
    return ret;
  }

  BookPageDimension getDimension(int pageNo) {
    if (dimensions == null) {
      if (!hasInfo) {
        return BookPageDimension(-1, -1);
      }
      Map<String, dynamic> info = jsonDecode(File(path.join(dir.path, 'info.json')).readAsStringSync());
      List<dynamic> dims = info['dimensions'];
      dimensions = dims.map((e) => BookPageDimension(e[0], e[1])).toList();
    }
    return dimensions![pageNo];
  }
}

class BookPageDimension {
  final int width;
  final int height;
  BookPageDimension(this.width, this.height);
}

String decrypt(DecryptData d) {
  final svgString = Utf8Decoder().convert(
    d.crypto.decryptSvg(d.svgCipher, d.pageKey)
  ).replaceAll('pt"', '"');
  return svgString;
}

class DecryptData {
  final DigilibCrypto crypto;
  final Uint8List svgCipher;
  final String pageKey;

  DecryptData(this.crypto, this.svgCipher, this.pageKey);
}
