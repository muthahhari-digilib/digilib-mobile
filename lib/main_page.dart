
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/widgets.dart';

import 'about_feedbck_page.dart';
import 'content_page.dart';
import 'history.dart';
import 'login.dart';
import 'main.dart';
import 'search_page.dart';
import 'backend_conn.dart';
import 'notification_page.dart';

class MainPage extends StatefulWidget {
  final BackendConnection backendConnection;

  const MainPage({Key? key, required this.backendConnection}) : super(key: key);

  @override
  State<StatefulWidget> createState() => MainPageState();
}

class MainPageState extends State<MainPage> {
  int _selectedTab = 0;

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    var theme = Theme.of(context);
    var bnbtheme = BottomNavigationBarTheme.of(context);
    var drawer = Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            child: Image.asset('images/logo.png')
          ),
          ListTile(
            title: Text('Notifikasi'),
            leading: Icon(Icons.notifications),
            onTap: () {
              Navigator.of(context)
                ..pop()
                ..push(MaterialPageRoute(builder: (context) => NotificationPage()));
            },
          ),
          ListTile(
            title: Text('Umpan Balik'),
            leading: Icon(Icons.feedback),
            onTap: () {
              Navigator.of(context)
                ..pop()
                ..push(MaterialPageRoute(builder: (context) => FeedbackPage()));
            },
          ),
          ListTile(
            title: Text('Tentang Aplikasi'),
            leading: Icon(Icons.info),
            onTap: () {
              Navigator.of(context)
                ..pop()
                ..push(MaterialPageRoute(builder: (context) => AboutPage()));
            },
          )
        ]
      ),
    );

    if (mq.orientation == Orientation.portrait) {
      return Scaffold(
        drawer: drawer,
        appBar: AppBar(
            title: Text(''),
            actions: [
              IconButton(icon: const Icon(Icons.search), onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SearchPage(backendConnection: widget.backendConnection,)));
              })
            ],
            flexibleSpace: Image.asset(
              'images/appbar_bg.png',
              alignment: Alignment.topCenter,
              fit: BoxFit.cover,
            ),
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: DigilibColors.secondary),
        ),
        extendBodyBehindAppBar: true,
        bottomNavigationBar: Container(child: BottomNavigationBar(
          elevation: 12,
          selectedItemColor: DigilibColors.primaryAlternative,
          currentIndex: _selectedTab,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Etalase'),
            BottomNavigationBarItem(icon: Icon(Icons.folder), label: 'Jelajahi'),
            BottomNavigationBarItem(icon: Icon(Icons.history), label: 'Riwayat'),
          ],
          onTap: (index) => setState(() => _selectedTab = index)
        ), decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(color: DigilibColors.secondary, blurRadius: 5)
          ],
        )),
        body: [
          getFeaturedWidget,
          getBrowseWidget,
          getHistoryWidget,
        ][_selectedTab](context)
      );
    } else {
      final selectedIconTheme = IconThemeData(color: DigilibColors.primaryAlternative);
      final unselectedIconTheme = IconThemeData(color: Colors.grey.shade700);
      final selectedLabelStyle = TextStyle(color: DigilibColors.primaryAlternative);
      final unselectedLabelStyle = TextStyle(color: Colors.grey.shade700);

      return Scaffold(
        drawer: drawer,
        appBar: AppBar(
            title: Text(''),
            actions: [
              IconButton(icon: const Icon(Icons.search), onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => SearchPage(backendConnection: widget.backendConnection,)));
              }),
              SizedBox(width: 80)
            ],
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: DigilibColors.secondary),
        ),
        extendBodyBehindAppBar: true,
        body: Row(children: [
          Expanded(child: [
            getFeaturedWidget,
            getBrowseWidget,
            getHistoryWidget,
          ][_selectedTab](context)),

          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: DigilibColors.secondary,
                  blurRadius: 7,
                  offset: Offset(0,0))
              ]
            ),
            child: Column(children: [
              SizedBox(height: 40),
              Spacer(),
              Expanded(
                child: TextButton(
                  child: Column(children:[
                    IconTheme(
                      data: _selectedTab == 0 ? selectedIconTheme : unselectedIconTheme,
                      child: Icon(Icons.home)
                    ),
                    Text('Etalase', style: _selectedTab == 0 ? selectedLabelStyle : unselectedLabelStyle)
                  ]),
                  onPressed: () => setState(()=>_selectedTab=0),
                )
              ),
              Expanded(
                child: TextButton(
                  child: Column(children:[
                    IconTheme(
                      data: (_selectedTab == 1 ? selectedIconTheme : unselectedIconTheme),
                      child: Icon(Icons.folder)
                    ),
                    Text('Jelajahi', style: _selectedTab == 1  ? selectedLabelStyle : unselectedLabelStyle)
                  ]),
                  onPressed: () => setState(()=>_selectedTab=1),
                )
              ),
              Expanded(
                child: TextButton(
                  child: Column(children:[
                    IconTheme(
                      data: (_selectedTab == 2 ? selectedIconTheme : unselectedIconTheme),
                      child: Icon(Icons.history)
                    ),
                    Text('Riwayat', style: _selectedTab == 2 ? selectedLabelStyle : unselectedLabelStyle)
                  ]),
                  onPressed: () => setState(()=>_selectedTab=2),
                )
              ),
              Spacer()
            ],),
          )
        ])
      );
    }
  }

  Widget getFeaturedWidget(BuildContext context) =>
    FutureBuilder<List<DigilibContent>>(
      key: Key('featured'),
        future: getHomeData(context),
        builder: (context, future) => homeBuilder(context, future, "Silakan search content di atas", contentBuilder: _homeEtalaseBuilder));
  
  Widget getHistoryWidget(BuildContext context) {
    final theme = Theme.of(context);
    return FutureBuilder<List<DigilibContent>>(
      key: Key('history'),
        future: getHistoryData(context),
        builder: (context, future) => homeBuilder(
          context, future, "Anda belum memiliki riwayat akses konten",
          title: Container(
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(14, 84, 14, 0),
          ),
          contentBuilder: _homeBuilder
          ));
  }
  Widget homeBuilder(BuildContext context, AsyncSnapshot<List<DigilibContent>> future, String emptyString, {Widget? title, required Widget Function(BuildContext, AsyncSnapshot<List<DigilibContent>>, String, bool) contentBuilder}) {
    var content = contentBuilder(context, future, emptyString, title != null);
    if (title == null || content is CircularProgressIndicator)
      return Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          //gradient: LinearGradient(colors: [const Color(0xff8a4000), const Color(0xffcf6d00)]),
          //image: DecorationImage(image: ExactAssetImage('images/background.jpg'), fit: BoxFit.cover), 
        ),
        child:content,
      );
    
    return Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          //gradient: LinearGradient(colors: [const Color(0xffffff00), const Color(0xffcf6d00)]),
          //image: DecorationImage(image: ExactAssetImage('images/background.jpg'), fit: BoxFit.cover), 
        ),
        child: Column(
          children: [
            //SizedBox(height:80),
            title,
            Expanded(child: content),
          ],
        ),
        height: double.infinity,
    );
  }

  void openContent(BuildContext context, DigilibContent content) {
    Navigator.of(context).push(
        MaterialPageRoute(
            builder: (context) => ContentPage(
                backendConnection: widget.backendConnection,
                content: content
            )
        )
    );
  }

  Widget _homeBuilder(BuildContext context, AsyncSnapshot<List<DigilibContent>> future, String emptyString, bool title) {
    if (future.connectionState == ConnectionState.waiting || future.connectionState == ConnectionState.active)
      return CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.white));
    
    if (future.hasError)
      return Center(child: Text('Error: ${future.error}'));

    const baseWidth = 360.0;
    final media = MediaQuery.of(context);
    final width = media.size.width;
    final factor = media.orientation == Orientation.portrait ? (width / baseWidth * 1.2) : 1.2;

    List<DigilibContent> contents = future.data!;

    if (contents.length == 0)
      return Container(
        padding: EdgeInsets.all(14),
        child: Text(emptyString),
        color: Colors.white
      );

    return ListView.builder(
      padding: title ? EdgeInsets.zero : null,
      itemCount: contents.length,
      itemExtent: factor * 96 + 4 + 14,
      itemBuilder: (context, i) => ContentWidget(
          backendConnection: widget.backendConnection,
          content: contents[i]
      ),
    );
  }


  Widget _homeEtalaseBuilder(BuildContext context, AsyncSnapshot<List<DigilibContent>> future, String emptyString, bool title) {
    if (future.connectionState == ConnectionState.waiting || future.connectionState == ConnectionState.active)
      return CircularProgressIndicator();
    
    if (future.hasError)
      return Center(child: Text('Error: ${future.error}'));

    List<DigilibContent> contents = future.data!;
    var books = contents.where((el) => el.contentType == ContentType.BOOK).toList();
    var audios = contents.where((el) => el.contentType == ContentType.AUDIO).toList();
    var photos = contents.where((el) => el.contentType == ContentType.PHOTO).toList();
    var videos = contents.where((el) => el.contentType == ContentType.VIDEO).toList();
    var quoteText;
    try {
      quoteText = photos.firstWhere((el) => el.topic == 'Quotes' && (el.description ?? '').contains('Pengirim: '))
                      .description?.split('Pengirim: ')[0].trim() ?? '';
    } on StateError catch(_) {
      quoteText = '';
    }
    var quoteStyle = Theme.of(context).primaryTextTheme.bodyText1?.apply(fontStyle: FontStyle.normal);

    return SingleChildScrollView(child: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/appbar_bg.png'),
          alignment: Alignment.topCenter,
          fit: BoxFit.contain
        ),
        color: DigilibColors.primaryAlternative
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 80),
          Padding(
            padding: EdgeInsets.fromLTRB(17, 0, 17, 0),
            child: Column(children: [
              Text.rich(TextSpan(children: [
              TextSpan(text: 'Quotes Harian\n', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text: '“', style: quoteStyle?.apply(fontSizeDelta: 12)),
              TextSpan(text: '\n'),
              TextSpan(text: quoteText),
              ], style: quoteStyle)),
              Align(alignment: Alignment.centerRight, child: Text('”', style: quoteStyle?.apply(fontSizeDelta: 12)))
            ])),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(top: Radius.circular(14))
            ),
            padding: EdgeInsets.fromLTRB(14, 19, 14, 14),
            child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
              ]
              + _group(context, "Koleksi Terbaru", contents, 12, '💬')
              + _group(context, "Buku & Artikel", books, 12, '💬book')
              + _group(context, "Audio", audios, 12, '💬audio')
              + _group(context, "Foto & Instagram", photos, 12, '💬video')
              + _group(context, "Video", videos, 12, '💬photo')
          ))
        ]
      )
    ));
  }

  List<Widget> _group(BuildContext context, String title, List<DigilibContent> contents, int count, String browsePath) {
    const baseWidth = 360.0;
    final media = MediaQuery.of(context);
    final theme = Theme.of(context);
    final width = media.size.width;
    final factor = media.orientation == Orientation.portrait ? (width / baseWidth * 1.9) : 1.9;

    return [
        Row(children: [
          Expanded(child: Text(title, style: theme.textTheme.headline6,),),
          InkWell(child: Row(
            children : [
              Text('Selengkapnya ', style: theme.textTheme.caption,),
              FaIcon(FontAwesomeIcons.arrowRight, color: DigilibColors.secondary, size: 10,)
            ]),
            onTap: () => setState(() {
              _selectedTab = 1;
              _goToPath(browsePath);
            })
            ),
        ]),
        SizedBox(height:12),
        Container(
          height: factor * 96 + 80,
          child: ListView.builder(
            itemCount: count,
            scrollDirection: Axis.horizontal,

            itemBuilder: (BuildContext context, int i) {
            final content = contents[i];
            return InkWell(
              onTap: () => openContent(context, content),
              child: Container(margin: EdgeInsets.only(right:12),
            child: Column(
              children: [
                Stack(children: [
                  Container(
                  child: Hero(tag: 'image${content.id}', child: ClipRRect(
                  borderRadius: BorderRadius.circular(4),
                  child: CachedNetworkImage(
                    alignment: Alignment.center,
                    fit: BoxFit.cover,
                    imageUrl: content.featuredImageUrl,
                    errorWidget: (context, url, res) => Container(
                      decoration: BoxDecoration(color: Colors.white),
                      padding: EdgeInsets.all(5),
                      child: Center(child: Image.asset('images/logo.png')),
                    ),
                  ))
                ),
              width: factor * 72,
              height: factor * 96,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black38),
                borderRadius: BorderRadius.circular(5)
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: ClipRRect(child:Text(
                  ' ${content.topic} ',
                  style: TextStyle(backgroundColor: Colors.teal, color: Colors.white, fontSize: 12,),
                ),
                borderRadius: BorderRadius.only(bottomRight: Radius.circular(5)),),
            )
            ]),
            Container(
              width: factor * 72,
              padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
              child: Text(
                content.name,
                overflow: TextOverflow.ellipsis,
                maxLines: 3
              ),
            ),
            ])));
          }
          ), 
        )
      ];
  }

  _updateFirebaseToken() async {
    var user = await widget.backendConnection.getCurrentUser();
    var token = await FirebaseMessaging.instance.getToken();
    if (token != null && token != user.firebaseTokenId) {
      user.firebaseTokenId = token;
      print('Change user token to $token');
      try {
        await widget.backendConnection.updateUserInfo(user);
      } catch (e) {
        // abaikan kalau nggak bisa update token, mungkin APInya belum dipasang
        print(e);
      }
    }
  }

  Future<List<DigilibContent>> getHomeData(BuildContext context) async {
    try {
      _updateFirebaseToken();
      return await widget.backendConnection.all();
    } on BackendError catch (err) {
      if (err.type == BackendErrorType.MustUpdate) {
        await showDialog(context: context, builder: (context) =>
          AlertDialog(
            content: Text("Maaf, Anda perlu mengupdate JR Digital Library App anda.",),
            actions: [
              TextButton(child:Text(
                err.cause.startsWith('https://play.google.com') ?
                'Buka Google Play' : 'Unduh Versi Baru'
              ), onPressed: () async {
                var url = err.cause;
                final launchable = await canLaunch(url);
                if (launchable) {
                  await launch(url);
                  SystemNavigator.pop();
                } else {
                  showDialog(context: context, builder: (context) =>
                  AlertDialog(
                    content: Text('Silakan buka ${err.cause} di browser anda.'),
                    actions: [
                      TextButton(child:Text('OK'), onPressed: () => SystemNavigator.pop(),)
                    ],
                  )
                );
                }
              },)
            ],
          ));
      } else if (err.type == BackendErrorType.InvalidSessionID) {
        await showDialog(context: context, builder: (context) =>
          AlertDialog(
            content: Text("Error: ${err.type} because ${err.cause}",),
            actions: [
              TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
            ],
          ));

        Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => LoginPage(
                    backendConnection: widget.backendConnection
                ))
        );
      } else if (err.type == BackendErrorType.MustRelogin) {
        await showDialog(context: context, builder: (context) =>
          AlertDialog(
            content: Text("Maaf, anda harus login ulang.",),
            actions: [
              TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
            ],
          ));

        Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => LoginPage(
                    backendConnection: widget.backendConnection
                ))
        );
      }
      throw err;
    }
  }

  Future<List<DigilibContent>> getHistoryData(BuildContext context) async {
    try {
      var history = await (await BrowseHistory.standard()).getHistory();
      var all = await widget.backendConnection.all();
      var ret = <DigilibContent>[];
      for (var item in history.reversed) {
        try {
          var a = all.firstWhere((element) {
            return element.path == item;
          });
          ret.add(a);
        } on StateError {
          // pass
        }
      }
      return ret;
    } on BackendError catch (err) {
      if (err.type == BackendErrorType.InvalidSessionID) {
        await showDialog(context: context, builder: (context) =>
          AlertDialog(
            content: Text("Error: ${err.type} because ${err.cause}",),
            actions: [
              TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
            ],
          ));

        Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => LoginPage(
                    backendConnection: widget.backendConnection
                ))
        );
      }
      throw err;
    } catch (err, st) {
      print(err);
      print(st);
      throw err;
    }
  }

  Future<List<dynamic>>? _browseFuture;
  String path = '';
  void _initGetBrowseData() {
    _browseFuture = _getBrowseData();
  }
  Future<List<dynamic>> _getBrowseData() {
    if (path == '💬') {
      return widget.backendConnection.home();
    }
    if (path.startsWith('💬')) {
      return (() async {
        var contentType = DigilibContent.parseContentType(path.substring(2));
        var all = await widget.backendConnection.all();
        return all.where((element) => element.contentType == contentType).toList();
      })();
    }
    return widget.backendConnection.list(path);
  }

  void _goToPath(String p) {
    setState(() {
      path = p;
      _initGetBrowseData();
    });
  }

  Widget getBrowseWidget(BuildContext context) {
    var theme = Theme.of(context);
    if (_browseFuture == null) {
      _initGetBrowseData();
    }
    var button = (String p, Widget w) =>
      InkWell(
        child: Container(
          height: 36,
          child: w,
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 7),
          margin: EdgeInsets.only(right: 3),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: DigilibColors.getPrimarySwatch().shade700,
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        onTap: () => _goToPath(p),
      );
    var buttons = [
      button('', Icon(Icons.home, size: 19, color: Theme.of(context).accentTextTheme.button?.color))
    ];
    var cp = '';
    for (var p in path.split('/')) {
      if (p == '') continue;
      cp += '/' + p;
      buttons.add(button(cp, Text(p, style: TextStyle(color: Theme.of(context).accentTextTheme.button?.color))));
    }
    Widget title = Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: 7, horizontal: 14),
        child: Center(
          child: Row(
            children: [
              Text('/ ', style: Theme.of(context).primaryTextTheme.bodyText1?.apply(fontSizeFactor: 1.4)),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(children: buttons)
              )
            ]
          )
        ),
    );
    Widget content = FutureBuilder(
      future: _browseFuture,
      builder: (context, future) {
        if (future.connectionState == ConnectionState.active || future.connectionState == ConnectionState.waiting)
          return Center(
            child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.brown))
          );
            
        if (future.hasError)
          return Center(child: Text('Error: ${future.error}'));

        if (future.data == null)
          return Center(child: Text('Error: data kosong'));

        var dirs = <String>[];
        var files = <DigilibContent>[];
        List<dynamic> data = future.data! as List;
        for (var el in data) {
          if (el is String)
            dirs.add(el);
          else if (el is DigilibContent)
            files.add(el);
        }
        dirs.sort();
        files.sort((a, b) => a.name.compareTo(b.name));

        return ListView.builder(
          padding: EdgeInsets.fromLTRB(0, 14, 0, 0),
          itemCount: files.length + 1,
          itemBuilder: (context, i) {
            if (i == 0) {
              return Wrap(
                alignment: WrapAlignment.center,
                children: dirs.map(
                  (dir) => InkWell(
                    onTap: () => _goToPath(path + '/' + dir),
                    child: Card(
                      child: Container(
                        width: 240,
                        padding: EdgeInsets.fromLTRB(19, 7, 19, 7),
                        child: Text(
                          dir,
                          style: theme.textTheme.headline6,
                          overflow: TextOverflow.ellipsis,
                        )
                      )
                    ),
                  ),
                ).toList(),
              );
            } else {
              return ContentWidget(backendConnection: widget.backendConnection, content: files[i-1]);
            }
          },
        );
      },
    );

    return WillPopScope(
      onWillPop: () async {
        if (path == '' || path == '/' || path.startsWith('💬')) {
          setState(()=>_selectedTab = 0);
          return false;
        }
        var p = path.substring(0, path.lastIndexOf('/'));
        _goToPath(p);
        return false;
      },
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/appbar_bg.png'),
            alignment: Alignment.topCenter,
            fit: BoxFit.cover
          ),
          color: DigilibColors.primaryAlternative
        ),
        child: Column(
          children: [
            SizedBox(height:80),
            title,
            Expanded(child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(top: Radius.circular(14)),
                color: Colors.white
              ),
              child: content)),
          ],
        ),
        height: double.infinity,
    ));
  }
}

class ContentWidget extends StatelessWidget {
  final BackendConnection backendConnection;
  final DigilibContent content;

  const ContentWidget({Key? key, required this.backendConnection, required this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const baseWidth = 360.0;
    final media = MediaQuery.of(context);
    final width = media.size.width;
    final factor = media.orientation == Orientation.portrait ? (width / baseWidth * 1.2) : 1.2;

    return InkWell(child: Padding(padding: EdgeInsets.fromLTRB(8,4,8,0),child:Card(
      color: Colors.white.withAlpha(200),
      child: Stack(
        children: [
        Container(
          padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Row(children: [
            Container(
                child: Hero(tag: 'image${content.id}', child: CachedNetworkImage(
                  alignment: Alignment.topCenter,
                  fit: BoxFit.cover,
                  imageUrl: content.featuredImageUrl,
                  errorWidget: (context, url, res) => Container(
                    decoration: BoxDecoration(color: Colors.brown),
                    child: Center(child: Image.asset('images/logo.png')),
                  ),
                )),
                decoration: BoxDecoration(border: Border.all(color: Colors.black12)),
                width: factor * 72,
                height: factor * 96),
            Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 7, right: 5),
                  height: factor * 96,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                  children:[
                  Text(content.name, maxLines: 2, overflow: TextOverflow.ellipsis, style: TextStyle(fontFamily: 'serif', fontSize: 19, color: DigilibColors.secondary)),
                  SizedBox(height: 3),
                  Text(content.authors.join(', '), maxLines: 2, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 14)),
                  Spacer(),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                    children: [
                      Text(
                        ' ${content.topic} ',
                        style: TextStyle(backgroundColor: Colors.teal, color: Colors.white, fontSize: 14),
                      ),
                      SizedBox(width: 5),
                    ],
                  ))
                ]))
            ),
            
          ])
        ),
        Positioned(
          right: 0,
          bottom: 0,
          child: Hero(tag: 'type${content.id}', child: Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomRight: Radius.circular(5)),
              color: content.typeColor
            ),
            child: Text(content.type, style: TextStyle(fontSize: 10))
          )),
        ),
        Positioned(
          bottom: 5,
          right: width - (factor * 72) - 8 - 8 - 7 - 5,
          child: Container(color: Colors.white60, child: Text(
            content.displayedContentLength,
            style: TextStyle(fontSize: 10),
          ))
        )
      ]))),
      onTap: () => openContent(context, content),
    );
  }

  void openContent(BuildContext context, DigilibContent content) {
    Navigator.of(context).push(
        MaterialPageRoute(
            builder: (context) => ContentPage(
                backendConnection: backendConnection,
                content: content
            )
        )
    );
  }
}

extension DisplayExtension on DigilibContent {
  Color get typeColor {
    return {
      'Buku': Colors.lightGreen,
      'Audio': Colors.lime,
      'Video': Colors.teal,
      'Lainnya': Color(0xEEEEEEFF)
    }[type] ?? Color(0xEEEEEEFF);
  }

  bool isBebasPinjam() => this.numberOfStocks == -1;
  bool isDapatDipinjam() => this.numberOfBorrowed < this.numberOfStocks;

  String get audioUrl => 'https://digilib.ijabi.my.id/content/audio/$id';
  String get bookUrl => 'https://digilib.ijabi.my.id/content/book/$id';
}

extension TagDisplayExtension on DigilibTag {
  static Color _fromHex(String? hexString) {
    if (hexString == null)
      return Color(0xFFDDDDDD);
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  Color get displayColor => _fromHex(color);
}