

enum BackendErrorType {
  ServerError,
  TimeoutError,
  ConnectionError,
  InvalidSessionID,
  ValidationError,
  MustUpdate,
  MustRelogin,
}

class BackendError implements Exception {
  String cause;
  BackendErrorType type;

  BackendError(this.type, this.cause);

  static BackendError server(String cause) => BackendError(BackendErrorType.ServerError, cause);
  static BackendError timeout(String cause) => BackendError(BackendErrorType.TimeoutError, cause);
  static BackendError connection(String cause) => BackendError(BackendErrorType.ConnectionError, cause);
  static BackendError invalidSession(String cause) => BackendError(BackendErrorType.InvalidSessionID, cause);
  static BackendError validation(String cause) => BackendError(BackendErrorType.ValidationError, cause);
  static BackendError mustUpdate(String cause) => BackendError(BackendErrorType.ValidationError, cause);
  static BackendError mustRelogin(String cause) => BackendError(BackendErrorType.MustRelogin, cause);

  String toString() => 'Error: $type because $cause';
}


abstract class BackendConnection {
  String? sessionID;

  /// session ID tidak diisi berarti belum ada session ID  
  BackendConnection({this.sessionID = ''});

  Map<String, String> get httpHeaders;

  // login & register
  
  /// Mengecek status registrasi no. hp [msisdn] ke server.
  /// Akan mengembalikan `true` jika terregistrasi, `false` jika belum.
  /// 
  /// OTP timeout default 5 menit setelah ini baru bisa dikirim.
  /// 
  /// Throws BackendErrorType
  Future<bool> checkMobilePhoneAndRegistration(String msisdn);

  /// Mengecek OTP [otp] yang dimasukkan user dengan apa yang tercatat di server untuk
  /// nomor HP [msisdn].
  /// 
  /// Jika nomor HP belum terdaftar, akan mengembalikan `tempRegisterSessionID`
  /// yaitu session ID sementara untuk registrasi.
  /// 
  /// Jika nomor HP telah terdaftar, akan mengembalikan `permanentSessionID`
  /// yaitu session ID permanen.
  /// 
  /// session ID tersebut juga disimpan pada member variable `sessionID` untuk pemanggilan
  /// selanjutnya
  ///
  /// Jika OTP salah, maka mengembalikan string kosong `""`.
  Future<String> checkOTP(String msisdn, String otp);

  /// Meminta server untuk mengirimkan kembali OTP ke nomor HP [msisdn]. Server membatasi
  /// pemanggilan OTP setiap 5 menit sekali, jadi fungsi ini tidak akan bisa dipanggil sebelum
  /// 5 menit.
  /// 
  /// Jika sudah boleh dan pengiriman OTP berhasil, maka akan mengembalikan jumlah detik sampai boleh
  /// meminta pengiriman lagi.
  /// 
  /// Jika belum boleh, maka akan throw BackendError dengan validation error
  Future<int> resendOTP(String msisdn);

  /// Mengirimkan form registrasi ke server.
  /// `sessionID` harus terset dengan tempRegisterSessionID, jika tidak akan ada error InvalidSessionID
  /// 
  /// Mengembalikan `permanentSessionID` jika berhasil.
  /// 
  /// sessionID tersebut juga akan disimpan pad amember variable `sessionID` untuk pemanggilan
  /// selanjutnya
  /// 
  /// throw BackendError dengan tipe ValidationError jika validasi gagal
  Future<String> register(String msisdn, RegistrationInfo registrationInfo, String firebaseTokenId);

  /// mendapatkan isi home screen dari server
  Future<List<DigilibContent>> home();

  /// mendapatkan seluruh content dari server
  Future<List<DigilibContent>> all();

  Future<List<dynamic>> list(String path);

  /// Searching data dari server
  Future<DigilibSearchResult> search(String query, DigilibSearchOptions options);

  Future<List<String>> getTypes();
  Future<Map<String, List<DigilibTag>>> getSearchTags();

  Future<UserInfo> getCurrentUser();
  Future<void> updateUserInfo(UserInfo user);
  Future<AppFeatureFlags> getAppFeatureFlags();
}

enum Gender {
  MALE,
  FEMALE
}

class RegistrationInfo {
  String name;
  Gender? gender;
  DateTime birthDate;
  String email = '';

  RegistrationInfo({required this.name, this.gender, required this.birthDate});
}

class UserInfo {
  String name;
  String? gender;
  DateTime birthDate;
  String email;
  String firebaseTokenId;

  UserInfo({required this.name, this.gender, required this.birthDate, required this.email, required this.firebaseTokenId});

  UserInfo.fromJson(Map<String, dynamic> json)
    : name = json['name'],
      gender = json['gender'],
      birthDate = DateTime.parse(json['birthDate'] ?? json['birth_date']),
      email = json['email'] ?? '',
      firebaseTokenId = json['firebaseTokenId'] ?? json['firebase_token_id'] ?? '';
  
  Map<String, dynamic> toJson() => {
    'name': name,
    'gender': gender,
    'birthDate': birthDate.toIso8601String(),
    'email': email,
    'firebaseTokenId': firebaseTokenId,
  };
}

enum ContentType {
  AUDIO,
  BOOK,
  VIDEO,
  PHOTO,
}

class DigilibContent {
  final String id;

  /// internal content type, untuk menangani tampilan
  final ContentType contentType;

  final String path;
  final String type;
  final String topic;
  final List<DigilibTag> tags;

  final List<String> authors;
  final String name;
  final String? description;
  
  final String featuredImageUrl;
  final String? originalPublisher;
  final String? sourceUrl;
  final String? copyright;
  final int numberOfBorrowed;
  final int numberOfStocks;
  final int contentLength;
  final bool isHighlighted;

  String get displayedContentLength {
    var itoa = (int i) => '$i'.padLeft(2, '0');
    switch(contentType) {
      case ContentType.AUDIO:
      case ContentType.VIDEO:
        return contentLength == -1 ? '00:00' : '${itoa(contentLength ~/ 60)}:${itoa(contentLength % 60)}';
      case ContentType.PHOTO:
        return '';
      default:
        return contentLength == -1 ? '' : '$contentLength halaman';
    }
  }

  DigilibContent({
    required this.id,
    required this.contentType,
    required this.path,
    required this.type,
    required this.topic,
    required this.tags,
    required this.authors,
    required this.name,
    this.description,
    required this.featuredImageUrl,
    this.originalPublisher,
    this.sourceUrl,
    this.copyright,
    required this.numberOfBorrowed,
    required this.numberOfStocks,
    required this.contentLength,
    required this.isHighlighted
  });

  DigilibContent.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        contentType = parseContentType(json['contentType'] ?? json['content_type']),
        path = json['path'],
        type = json['type'],
        topic = json['topic'],
        tags = _parseTags(json['tags']) ?? _getAllTags(json),
        featuredImageUrl = json['featuredImageUrl'] ?? json['featured_image'],
        originalPublisher = json['originalPublisher'] ?? json['original_publisher'],
        sourceUrl = json['sourceUrl'] ?? json['source_url'],
        copyright = json['copyright'],
        numberOfBorrowed = json['numberOfBorrowed'] ?? json['number_of_borrowed'] ?? 0,
        numberOfStocks = json['numberOfStocks'] ?? json['number_of_stocks'] ?? -1,
        contentLength = json['contentLength'] ?? json['content_length'] ?? -1,
        authors = _parseAuthors(json['authors'] ?? json['author']?.split(';') ?? []),
        name = json['name'] ?? _getName(json['file_name']) ?? '',
        description = json['description'],
        isHighlighted = json['isHighlighted'] ?? json['is_highligted'] ?? json['is_highlight'] ?? false;
  
  Map<String, dynamic> toJson() => {
    'id': id,
    'contentType': contentTypeToString(contentType),
    'path': path,
    'type': type,
    'topic': topic,
    'tags': tags,
    'featuredImageUrl': featuredImageUrl,
    'originalPublisher': originalPublisher,
    'sourceUrl': sourceUrl,
    'copyright': copyright,
    'numberOfBorrowed': numberOfBorrowed,
    'numberOfStocks': numberOfStocks,
    'contentLength': contentLength,
    'authors': authors,
    'name': name,
    'description': description,
    'isHighlighted': isHighlighted,
  };

  static String? contentTypeToString(ContentType contentType) {
    return {
      ContentType.AUDIO: 'audio',
      ContentType.BOOK: 'book',
      ContentType.VIDEO: 'video',
      ContentType.PHOTO: 'photo',
    }[contentType];
  }

  static List<DigilibTag> _getAllTags(Map<String, dynamic> json) =>
     []
      ..addAll(_splitToIter(json['month'], 'month'))
      ..addAll(_splitToIter(json['event_name'], 'event'))
      ..addAll(_splitToIter(json['people_name'], 'people'))
      ..addAll(_splitToIter(json['year'], 'year'));

  static Iterable<DigilibTag> _splitToIter(String? author, String type) {
    return author?.split(';').where((e) => e.trim() != '')
              .map((e) => DigilibTag(type, e))
        ?? Iterable<DigilibTag>.empty();
  }

  static ContentType parseContentType(String str) {
    if (str == 'audio') return ContentType.AUDIO;
    if (str == 'book')  return ContentType.BOOK;
    if (str == 'video') return ContentType.VIDEO;
    if (str == 'photo') return ContentType.PHOTO;
    throw BackendError.server('Undefined content_type ' + str);
  }

  static String _getName(String filename) {
    var fn = filename;
    if (fn.contains('/'))
      fn = fn.split('/').last;
    if (fn.contains('.'))
      fn = (fn.split('.')..removeLast()).join('.');
    fn = fn.replaceAll('_', '—');
    return fn;
  }

  static List<DigilibTag>? _parseTags(List<dynamic>? json) {
    return json?.map((el) {
      Map<String, dynamic> o = el;
      return DigilibTag(o['type'], o['tag'], o['color']);
    }).toList();
  }

  static List<String> _parseAuthors(List<dynamic> json) {
    return json.map((e) => e.toString()).toList();
  }
}

class DigilibTag {
  final String type;
  final String tag;
  String? color;

  DigilibTag(this.type, this.tag, [this.color]);

  Map<String, dynamic> toJson() => {
    'type': type,
    'tag': tag,
    'color': color
  };
}


class DigilibSearchOptions {
  static const List<String> EMPTY_TYPES = [];
  /// page dari 1
  final int page;
  final List<String> type;

  DigilibSearchOptions({this.page = 0, this.type = EMPTY_TYPES});
}


class DigilibSearchResult {
  final int totalResult;
  /// page dari 1
  /// paging pasti 50
  final int page;
  final List<DigilibContent> contents;

  DigilibSearchResult(this.totalResult, this.page, this.contents);
  DigilibSearchResult.fromJson(Map<String, dynamic> json)
    :  totalResult = json['totalResult'],
       page = json['page'],
       contents = _parseContents(json['contents']);
  
  static List<DigilibContent> _parseContents(List<dynamic> json) {
    return json.map((e) {
      Map<String, dynamic> o = e;
      return DigilibContent.fromJson(o);
    }).toList();
  }
}


class AppFeatureFlags {
  bool peminjaman;

  AppFeatureFlags({required this.peminjaman});

  AppFeatureFlags.fromJson(Map<String, dynamic> json)
    : peminjaman = json['peminjaman'] ?? false;
}
