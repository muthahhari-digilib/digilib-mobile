

import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PhotoPage extends StatelessWidget {
  final String imageUrl;

  const PhotoPage({Key? key, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black38,
        shadowColor: Colors.transparent,
      ),
      extendBodyBehindAppBar: true,
      body: ExtendedImage.network(
        imageUrl,
        height: double.infinity,
        mode: ExtendedImageMode.gesture,
        initGestureConfigHandler: (ExtendedImageState state) {
          return GestureConfig(
            minScale: 0.9,
            animationMinScale: 0.7,
            maxScale: 4.0,
            animationMaxScale: 4.5,
            speed: 1.0,
            inertialSpeed: 100.0,
            initialScale: 1.0,
            inPageView: false,
            initialAlignment: InitialAlignment.center,
            gestureDetailsIsChanged: (GestureDetails? details) {
              //print(details?.totalScale);
            },
          );
        }
      )
    );
  }
}