
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:intl/intl.dart';

import 'backend_conn.dart';

import 'package:http/http.dart' as http;

class BackendConnectionImpl extends BackendConnection {
  static const SERVER_URL = "https://digilib.ijabi.my.id";
  String clientVersion;

  BackendConnectionImpl(String this.clientVersion);
  
  @override
  Map<String, String> get httpHeaders => sessionID != null ? {
    'Authorization': 'Bearer $sessionID',
    'Client-Version': clientVersion
  } : {
    'Client-Version': clientVersion
  };

  // FIXME: header + 204
  Future<http.Response> doPost(String path, dynamic jsonBody) async {
    try {
      var res = await http.post(
        Uri.parse(SERVER_URL + path),
        body: jsonEncode(jsonBody),
        headers: httpHeaders..putIfAbsent('Content-Type', () => 'application/json')
      );
      _checkStatusCode(res);
      return res;
    } catch (e) {
      throw BackendError.connection(e.toString());
    }
  }

  Future<http.Response> doPut(String path, dynamic jsonBody) async {
    try {
      var res = await http.put(
        Uri.parse(SERVER_URL + path),
        body: jsonEncode(jsonBody),
        headers: httpHeaders..putIfAbsent('Content-Type', () => 'application/json')
      );
      _checkStatusCode(res);
      return res;
    } catch (e) {
      throw BackendError.connection(e.toString());
    }
  }

  Future<http.Response> doGet(String path, [Map<String, String>? parameters]) async {
    var uri = Uri.parse(SERVER_URL + path);
    final Map<String, dynamic> params = {};
    params.addAll(uri.queryParameters);
    params.addAll(parameters ?? {});
    uri = uri.replace(queryParameters: params);
    try {
      var res = await http.get(
        uri,
        headers: httpHeaders..putIfAbsent('Content-Type', () => 'application/json'),
      );
      _checkStatusCode(res);
      return res;
    } on BackendError catch (e) {
      throw e;
    } catch (e) {
      throw BackendError.connection(e.toString());
    }
  }

  void _checkStatusCode(http.Response res) {
    if (res.statusCode == 204) {
      Map<String, dynamic> obj = jsonDecode(utf8.decode(res.bodyBytes));
      throw BackendError.mustUpdate(obj['message']);
    } else if (res.statusCode == 401) {
      Map<String, dynamic> obj = jsonDecode(utf8.decode(res.bodyBytes));
      throw BackendError.mustRelogin(obj['message']);
    } else if (res.statusCode == 404) {
      throw BackendError.connection("Server returned 404 Not Found");
    }
  }

  @override
  Future<bool> checkMobilePhoneAndRegistration(String msisdn) async {
    var res = await doPost('/user/is-registered', {
        'phone_number': msisdn
    });
    if (res.statusCode == 200) {
      Map<String, dynamic> obj = jsonDecode(res.body);
      return obj['is_registered'];
    } else if (res.statusCode == 400) {
      Map<String, dynamic> obj = jsonDecode(res.body);
      throw BackendError.validation(obj['message']);
    } else {
      throw BackendError.server('Invalid statusCode ${res.statusCode}');
    }
  }

  @override
  Future<String> checkOTP(String msisdn, String otp) async {
    var res = await doPost('/user/otp', {
        'phone_number': msisdn,
        'otp': otp
    });
    if (res.statusCode == 200) {
      Map<String, dynamic> obj = jsonDecode(res.body);
      this.sessionID = obj['jwt'];
      return obj['jwt'];
    } else if (res.statusCode == 400) {
      Map<String, dynamic> obj = jsonDecode(res.body);
      throw BackendError.validation(obj['message']);
    } else {
      throw BackendError.server('Invalid statusCode ${res.statusCode}');
    }
  }

  @override
  Future<List<DigilibContent>> home() async {
    var res = await doGet('/home/');
    return _processHomeAndSearchResult(res);
  }

  @override
  Future<List<DigilibContent>> all() async {
    var res = await doGet('/home/all/');
    return _processHomeAndSearchResult(res);
  }

  List<DigilibContent> _processHomeAndSearchResult(http.Response res) {
    if (res.statusCode == 200) {
      List<dynamic> obj = jsonDecode(utf8.decode(res.bodyBytes));
      return obj.map((e) {
        final content = DigilibContent.fromJson(e);
        // TODO: set color to tags
        return content;
      }).toList();
    } else if (res.statusCode == 400) {
      Map<String, dynamic> obj = jsonDecode(res.body);
      throw BackendError.validation(obj['message']);
    } else {
      throw BackendError.server('Invalid statusCode ${res.statusCode}: ${res.body}');
    }
  }

  @override
  Future<String> register(String msisdn, RegistrationInfo reg, String firebaseTokenId) async {
    var res = await doPost('/user/register', {
        'phone_number': msisdn,
        'name': reg.name,
        'birth_date': DateFormat('yyyy-MM-dd').format(reg.birthDate),
        'email': reg.email,
        'gender': reg.gender == null ? null 
                  : reg.gender == Gender.MALE ? '1' : '2',
        'firebase_token_id': firebaseTokenId
    });
    if (res.statusCode == 201) {
      Map<String, dynamic> obj = jsonDecode(res.body);
      if (obj.containsKey('jwt'))
        this.sessionID = obj['jwt'];

      return this.sessionID!;
    } else if (res.statusCode == 400) {
      Map<String, dynamic> obj = jsonDecode(res.body);
      throw BackendError.validation(obj['message']);
    } else {
      throw BackendError.server('Invalid statusCode ${res.statusCode}');
    }
  }

  @override
  Future<int> resendOTP(String msisdn) async {
    var _ = await checkMobilePhoneAndRegistration(msisdn);
    return 600;
  }

  @override
  Future<DigilibSearchResult> search(String query, DigilibSearchOptions options) async {
    var res = await doGet('/search/', {
      'q': query,
      'type': options.type.join(','),
      'page': options.page.toString()
    });
    
    // TODO: search API di server harus disesuaikan untuk pagination
    List<DigilibContent> list = _processHomeAndSearchResult(res);
    return DigilibSearchResult(
      int.tryParse(res.headers['X-JR-DIGILIB-TotalRecords'] ?? 'NOLEN') ?? list.length,
      int.tryParse(res.headers['X-JR-DIGILIB-Page'] ?? 'NODATA') ?? 1,
      list
    );
  }

  @override
  Future<Map<String, List<DigilibTag>>> getSearchTags() async {
    final res = await doGet('/content/tags');
    
    Map<String, dynamic> json = jsonDecode(res.body);
    
    return json.map((k,v) {
      if (v is String)
        return MapEntry(k, [DigilibTag(k, v)]);
      else if (v is List)
        return MapEntry(k, v.map((e) {
          if (e is Map)
            return DigilibTag(k, e['tag'] ?? '?', e['color']);
          else
            return DigilibTag(k, '$e');
        }).toList());
      else
        return MapEntry(k, []);
    });
  }

  @override
  Future<List<String>> getTypes() async {
    final res = await doGet('/content/types');
    return (jsonDecode(res.body) as List<dynamic>).map((e) => e as String).toList();
  }

  @override
  Future<List> list(String path) {
    // TODO in the future
    throw UnimplementedError();
  }

  @override
  Future<AppFeatureFlags> getAppFeatureFlags() async {
    try {
      final res = await doGet('/app-feature-flags/');
      return AppFeatureFlags.fromJson(jsonDecode(res.body));
    } on BackendError catch (_) {
      return AppFeatureFlags(peminjaman: false);
    }
  }

  @override
  Future<UserInfo> getCurrentUser() async {
    try {
      final res = await doGet('/current_user/');
      return UserInfo.fromJson(jsonDecode(res.body));
    } on BackendError catch (_) {
      return UserInfo(
        name: '',
        birthDate: DateTime.now(),
        email: '',
        firebaseTokenId: '',
      );
    }
  }

  @override
  Future<void> updateUserInfo(UserInfo user) async {
    await doPut('/current_user/', user.toJson());
  }

}


/// Backend connection yang didukung local cache, untuk 
class LocalCacheBackendConnection extends BackendConnection {
  final File localCacheFile;
  final List<DigilibContent> contents;

  LocalCacheBackendConnection(this.localCacheFile) :
    contents = _loadFile(localCacheFile); 
  
  static List<DigilibContent> _loadFile(File localCacheFile) {
    if (!localCacheFile.existsSync())
      return [];
    var str = localCacheFile.readAsStringSync();
    List<dynamic> obj = jsonDecode(str);
    return obj.map((e) {
        final content = DigilibContent.fromJson(e);
        // TODO: set color to tags
        return content;
      }).toList();
  }
  
  void updateContents(List<DigilibContent> neo) {
    contents..clear()
            ..addAll(neo);
    localCacheFile.writeAsString(jsonEncode(neo));
  }

  @override
  Future<List<DigilibContent>> all() async {
    return UnmodifiableListView(contents);
  }

  @override
  Future<bool> checkMobilePhoneAndRegistration(String msisdn) {
    throw UnimplementedError();
  }
  
  @override
  Future<String> checkOTP(String msisdn, String otp) {
    throw UnimplementedError();
  }
  
  @override
  Future<Map<String, List<DigilibTag>>> getSearchTags() async {
    var ret = <String, List<DigilibTag>>{};
    for (var e in contents) {
      for (var tag in e.tags) {
        if (!ret.containsKey(tag.type))
          ret[tag.type] = [];
        ret[tag.type]!.add(tag);
      }
      // TODO author dsb
    }
    return ret;
  }
  
  @override
  Future<List<String>> getTypes() async {
    return contents.map((c) => c.type).toSet().toList();
  }
  
  @override
  Future<List<DigilibContent>> home() async {
    return contents.where((e) => e.isHighlighted).toList();
  }
  
  @override
  Map<String, String> get httpHeaders => throw UnimplementedError();
  
  @override
  Future<String> register(String msisdn, RegistrationInfo registrationInfo, String firebaseTokenId) {
    throw UnimplementedError();
  }
  
  @override
  Future<int> resendOTP(String msisdn) {
    throw UnimplementedError();
  }
  
  @override
  Future<DigilibSearchResult> search(String query, DigilibSearchOptions options) async {
    Iterable<String> token = query.split(" ").map((e) => e.toLowerCase());
    var res = contents.where((e) =>
      token.every((q) =>
        e.name.toLowerCase().contains(q) ||
        (e.description ?? '').toLowerCase().contains(q) ||
        e.authors.any((author) => author.toLowerCase().contains(q)) ||
        e.path.toLowerCase().contains(q) ||
        e.topic.toLowerCase().contains(q) ||
        e.tags.any((tag) => tag.type.contains(q) || tag.tag.contains(q))
      )
    ).toList();

    return DigilibSearchResult(
      res.length,
      1,
      res
    );
  }

  @override
  Future<List> list(String path) async {
    return <dynamic>[] +
           contents.map((e) => e.path).toSet()
                   .where((e) => e.startsWith('$path/'))
                   .map((e) => e.substring(path.length + 1).split('/')[0])
                   .toSet().toList() +
           contents.where((e) => e.path == path)
                   .toList();
  }

  @override
  Future<AppFeatureFlags> getAppFeatureFlags() {
    throw UnimplementedError();
  }

  @override
  Future<UserInfo> getCurrentUser() {
    throw UnimplementedError();
  }

  @override
  Future<void> updateUserInfo(UserInfo user) {
    throw UnimplementedError();
  }
}

class AutoSwitchBackendConnection extends BackendConnection {
  final BackendConnection impl;
  final LocalCacheBackendConnection bak;
  final Duration retryTime;
  DateTime lastRetry = DateTime(2000);
  bool backendIsActive = true;
  
  Future<List<DigilibContent>>? allFuture;

  AutoSwitchBackendConnection(
      BackendConnection backendConnection,
      File localCacheFile, {this.retryTime = const Duration(hours: 1)}):
    impl = backendConnection,
    bak = LocalCacheBackendConnection(localCacheFile);

  @override
  Future<List<DigilibContent>> all() {
    if (allFuture != null) {
      return allFuture!;
    }
    if (lastRetry.add(retryTime).isAfter(DateTime.now())) {
      return bak.all();
    }
    allFuture = impl.all()
    .then((value) {
      try {
        bak.updateContents(value);
      } catch (e, s) {
        print(e);
        print(s);
      }
      return value;
    })
    .onError((error, stackTrace) => bak.all())
    .then((value) {
      allFuture=null;
      lastRetry = DateTime.now();
      return value;
    });
    return allFuture!;
  }

  @override
  Future<bool> checkMobilePhoneAndRegistration(String msisdn) => impl.checkMobilePhoneAndRegistration(msisdn);

  @override
  Future<String> checkOTP(String msisdn, String otp) => impl.checkOTP(msisdn, otp);
  
  @override
  Future<Map<String, List<DigilibTag>>> getSearchTags() => _callIf(impl.getSearchTags, bak.getSearchTags);
    
  @override
  Future<List<String>> getTypes() => _callIf(impl.getTypes, bak.getTypes);
    
  @override
  Future<List<DigilibContent>> home() => _callIf(impl.home, bak.home);
    
  @override
  Map<String, String> get httpHeaders => impl.httpHeaders;
    
  @override
  Future<String> register(String msisdn, RegistrationInfo registrationInfo, String firebaseTokenId) => impl.register(msisdn, registrationInfo, firebaseTokenId);
    
  @override
  Future<int> resendOTP(String msisdn) => impl.resendOTP(msisdn);

  @override
  Future<DigilibSearchResult> search(String query, DigilibSearchOptions options) =>
    _callIf(() => impl.search(query, options), () => bak.search(query, options));
  
  Future<T> _callIf<T>(Future<T> Function() implFunc, Future<T> Function() bakFunc) async {
    if (backendIsActive || lastRetry.add(retryTime).isBefore(DateTime.now())) {
      try {
        var res = await implFunc();

        impl.all();

        return res;
      } on BackendError catch(e) {
        if (e.type == BackendErrorType.ConnectionError
            || e.type == BackendErrorType.ServerError
            || e.type == BackendErrorType.TimeoutError) {
          // di bawah ambilnya
          backendIsActive = false;
          lastRetry = DateTime.now();
        } else {
          throw e;
        }
      }
    }
    return await bakFunc();
  }

  @override
  Future<List> list(String path) {
    return bak.list(path);
  }

  @override
  Future<AppFeatureFlags> getAppFeatureFlags() {
    return impl.getAppFeatureFlags();
  }

  @override
  Future<UserInfo> getCurrentUser() {
    return impl.getCurrentUser();
  }

  @override
  Future<void> updateUserInfo(UserInfo user) {
    return impl.updateUserInfo(user);
  }
}