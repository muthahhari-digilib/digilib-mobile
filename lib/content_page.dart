import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:digilib/audio_page.dart';
import 'package:digilib/book_page.dart';
import 'package:digilib/config_store.dart';
import 'package:digilib/digilibcrypto.dart';
import 'package:digilib/history.dart';
import 'package:digilib/photo_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'backend_conn.dart';
import 'main_page.dart';
import 'package:flutter/widgets.dart';

class ContentPage extends StatelessWidget {
  final BackendConnection backendConnection;
  final DigilibContent content;

  const ContentPage(
      {Key? key, required this.backendConnection, required this.content})
      : super(key: key);

  Widget topImage(BuildContext context) => Stack(alignment: AlignmentDirectional.center, children: [
          Hero(
              tag: 'image${content.id}',
              child: Container(
                decoration: BoxDecoration(color: Colors.brown),
                child: CachedNetworkImage(
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                  width: double.infinity,
                  height: 320,
                  imageUrl: content.featuredImageUrl,
                  errorWidget: (context, url, res) => Container(
                    decoration: BoxDecoration(color: Colors.brown),
                    width: double.infinity,
                    height: double.infinity,
                    child: Center(child: Image.asset('images/logo.png')),
                  ),))),
          Positioned(
            right: 0,
            bottom: 0,
            child: Hero(
                tag: 'type${content.id}',
                child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(color: content.typeColor),
                    child: Text(content.type, style: TextStyle(fontSize: 12)))),
          ),
        ]
        /*+ (content.isBebasPinjam() ? [
            FloatingActionButton(child: Icon(
              (content.contentType == ContentType.VIDEO || content.contentType == ContentType.AUDIO)
              ? Icons.play_arrow : Icons.menu_book,
            ), onPressed: () => openContent(context))
          ] : []
        )*/
      );

  List<Widget> imageWidgets(BuildContext context) => [
    content.isBebasPinjam() ? InkWell(
          child: topImage(context), onTap: () => openContent(context),
          splashColor: Colors.green,
          highlightColor: Colors.blue,
        ) : topImage(context),
  ];

  List<Widget> descriptionWidgets(BuildContext context) => [
        Padding(
          padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
          child: Text(content.name,
              style: TextStyle(fontFamily: 'Roboto', fontSize: 18)),
        ),
        Padding(
            padding: EdgeInsets.fromLTRB(8, 5, 8, 0),
            child: SizedBox(
              child: Text(
                content.authors.join(', '),
                style: TextStyle(fontSize: 15),
              ),
            )),

        Padding(
          padding: EdgeInsets.fromLTRB(8, 12, 8 ,12),
          child: Row(children: [
            Expanded(
              child: Column(children: [
                FaIcon(FontAwesomeIcons.tag),
                Text(content.topic)
              ],),
            ),
            VerticalDivider(color:Colors.black,),
            Expanded(
              child: Column(children: [
                FaIcon({
                  ContentType.AUDIO: FontAwesomeIcons.podcast,
                  ContentType.BOOK: FontAwesomeIcons.fileAlt,
                  ContentType.VIDEO: FontAwesomeIcons.youtube,
                  ContentType.PHOTO: FontAwesomeIcons.image
                }[content.contentType]),
                Text(content.type)
              ],),
            ),
            VerticalDivider(color:Colors.black),
            Expanded(
              child: Column(children: [
                FaIcon({
                  ContentType.AUDIO: FontAwesomeIcons.clock,
                  ContentType.VIDEO: FontAwesomeIcons.clock,
                  ContentType.BOOK: FontAwesomeIcons.copy,
                  ContentType.PHOTO: FontAwesomeIcons.photoVideo,
                }[content.contentType]),
                Text(content.displayedContentLength)
              ],),
            ),
          ],)
        ),

        // TAGS
        Padding(
            padding: EdgeInsets.fromLTRB(8, 5, 8, 12),
            child: Wrap(
              runSpacing: 5,
              children: []..addAll((content.tags..sort((a,b)=>a.tag.compareTo(b.tag))).map((tag) => Padding(
                  padding: EdgeInsets.only(right: 2),
                  child: Text(
                    ' ${tag.tag} ',
                    style: TextStyle(
                        backgroundColor: tag.displayColor,
                        color: Colors.black,
                        fontSize: 12,
                        ),
                  )))),
            )),
        Padding(
          padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
          child: SizedBox(
            child: Text(
              content.description ?? '',
              style: TextStyle(fontSize: 14),
            ),
          ),
        ),
      ];

  List<Widget> bebasPinjamWidgets(BuildContext context) => <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
          child: Text(
              'Koleksi Digilib ini dapat dibuka secara bebas tanpa Peminjaman'),
        ),
      ] + ((content.contentType != ContentType.VIDEO) ? [
        Padding(
          padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
          child: ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.green),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ))),
            child: Text(
              'Buka'
            ),
            onPressed: () => openContent(context)
          ),
        ),
      ] : []) + openSourceWidgets(context) + [
        SizedBox(height: 40)
      ];

  List<Widget> get pinjamWidgets => <Widget>[
        Container(
          child: Row(
            children: [
              Container(
                padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4.0),
                child: SizedBox(
                  child: Text(
                    'Jumlah Ketersediaan: ',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                width: 200,
                child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.grey),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        ))),
                    child: Text(
                      'Pinjam',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () {
                      // TODO: fungsi menyusul
                    }),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 8.0, left: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                width: 200,
                child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.grey),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14.0),
                        ))),
                    child: Text(
                      'Pesan',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () {
                      //TODO: fungsi menyusul
                    }),
              ),
            ],
          ),
        )
      ];
    
  List<Widget> openSourceWidgets(BuildContext context) {
    BrowseHistory.standard().then((h) => h.addHistory(content.path));
    if (content.sourceUrl == null) return [];

    var text = Text('Buka Link Eksternal');
    Widget icon = Icon(Icons.open_in_new);
    var color = Colors.grey;

    if (
        content.sourceUrl!.startsWith('https://www.youtube.com/') ||
        content.sourceUrl!.startsWith('https://youtube.com/') ||
        content.sourceUrl!.startsWith('https://youtu.be/')) {
      text = Text('Buka di YouTube');
      color = Colors.red;
      icon = FaIcon(FontAwesomeIcons.youtube);
    } else if (content.contentType == ContentType.VIDEO) {
      text = Text('Buka di YouTube');
      color = Colors.red;
      icon = FaIcon(FontAwesomeIcons.youtube);
    } else if (content.sourceUrl!.startsWith('https://www.instagram.com/') ||
               content.sourceUrl!.startsWith('https://instagram.com/')) {
      text = Text('Buka di Instagram');
      color = Colors.orange;
      icon = FaIcon(FontAwesomeIcons.instagram);
    }
    return [
      Padding(
        padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
        child: ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(color),
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              )),
              ),
          child: Row(children: [text, icon], mainAxisSize: MainAxisSize.min,),
          onPressed: () => openSourceUrl(context)
        ),
      ),
    ];
  }

  void openSourceUrl(BuildContext context) async {
    var url = content.sourceUrl!;
    if (content.contentType == ContentType.VIDEO &&
        !url.startsWith('https://'))
      url = 'https://youtu.be/${content.sourceUrl}';

    final launchable = await canLaunch(url);
    if (launchable) {
      await launch(url);
    } else {
      showDialog(context: context, builder: (context) =>
        AlertDialog(
          content: Text('Tidak dapat membuka'),
          actions: [
            TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
          ],
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: media.orientation == Orientation.portrait ? Colors.black26 : Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        extendBodyBehindAppBar: true,
        body: media.orientation == Orientation.portrait ?
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: 
                  <Widget>[SizedBox(height:96)] +
                  imageWidgets(context) +
                  descriptionWidgets(context) +
                  (content.isBebasPinjam() ? bebasPinjamWidgets(context) : pinjamWidgets),
            )
          )
          :
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 36),
                child: IconButton(icon: Icon(Icons.arrow_back), onPressed: () => Navigator.of(context).pop(),),
              ),
              Expanded(
              child: SingleChildScrollView(child: Padding(
                padding: EdgeInsets.only(top:36, right: 14), child: Column(
                 crossAxisAlignment: CrossAxisAlignment.stretch,
                 children: descriptionWidgets(context) +
                   (content.isBebasPinjam() ? bebasPinjamWidgets(context) : pinjamWidgets),
              ))),
              //width: media.size.width - media.size.width * 3 / 8,
              //height: double.infinity
            )] + [Container(
              width: media.size.width * 3 / 8,
              padding: EdgeInsets.all(5),
              alignment: Alignment.center,
              color: Colors.brown,
              child: Column(
                mainAxisSize: MainAxisSize.max,
              children: <Widget>[Spacer()] + imageWidgets(context) + [Spacer()]
            ))],
          )
        );
  }

  void openContent(BuildContext context) async {
    try {
    var page;
    switch (content.contentType) {
      case ContentType.VIDEO: {
        page = null;
        break;
      }
      case ContentType.PHOTO: {
        page = PhotoPage(imageUrl: content.featuredImageUrl);
        break;
      }
      case ContentType.AUDIO: {
        page = AudioPage(
          audioUrl: content.audioUrl, title: content.name,
          artworkUrl: content.featuredImageUrl, headers: backendConnection.httpHeaders,
        );
        break;
      }
      case ContentType.BOOK: {
        var privateKey = await ConfigStore().getString('PRIVATE_KEY');
        var publicKey = await ConfigStore().getString('PUBLIC_KEY');
        if (privateKey == null) {
          var keys = DigilibCrypto.generateMobilePhoneKeys();
          ConfigStore()
            ..setString('PRIVATE_KEY', keys.privateKey)
            ..setString('PUBLIC_KEY', keys.publicKey);
          privateKey = keys.privateKey;
          publicKey = keys.publicKey;
        }

        final dir = await getTemporaryDirectory();
        final zipFile = File(join(dir.path, '${content.id}.zip'));
        final infoFile = File(join(dir.path, '${content.id}.info'));
        BookInfo? info;

        if (infoFile.existsSync()) {
          var json = jsonDecode(infoFile.readAsStringSync());
          info = BookInfo.fromJson(json);

          if (info.returnDate.isBefore(DateTime.now())) {
            zipFile.deleteSync();
            infoFile.deleteSync();
            info = null;
          }
        }

        if (info == null) {
          var uri = Uri.parse(content.bookUrl);
          var dialog = DownloadDialog(
            headers: backendConnection.httpHeaders..putIfAbsent(
                        'X-JR-DIGILIB-EncryptionKey', () => publicKey!),
            url: uri,
            outputFile: zipFile,
          );
          info = await showDialog(
            context: context, builder: (context) => dialog,
            barrierDismissible: false
          );
          if (info == null)
            return;
          
          infoFile.writeAsStringSync(jsonEncode(info));
        }

        page = DigilibBookPage(
          bookZipFile: zipFile, bookTitle: content.name,
          bookKeys: info.bookKeys, mobilePhonePrivateKey: privateKey,
          returnDate: info.returnDate, bookLen: content.contentLength,);
      }
    }
    if (page == null) {
      openSourceUrl(context);
    } else {
      (await BrowseHistory.standard()).addHistory(content.path);
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => page));
    }
    } catch (e, s) {
      print(s);
      showDialog(
        context: context,
        builder: (contexT) => AlertDialog(
          content: Text('Error: $e'),
          actions: [
            TextButton(onPressed: () => Navigator.of(context).pop(), child: Text('OK'))
          ],
        )
      );
    }
  }
}

class DownloadDialog extends StatefulWidget {
  final Uri url;
  final Map<String, String> headers;
  final File outputFile;

  const DownloadDialog({Key? key, required this.url, required this.headers, required this.outputFile}) : super(key: key);

  @override
  State<DownloadDialog> createState() => DownloadDialogState();
}

class DownloadDialogState extends State<DownloadDialog> {
  double progress = 0.0;
  StreamSubscription? s;

  @override
  void initState() {
    super.initState();
  }

  Future<void> download(BuildContext context) async {
    final client = new HttpClient();
    final file = widget.outputFile;

    var request = await client.getUrl(widget.url);
    widget.headers.forEach(request.headers.add);
    request.headers.add(HttpHeaders.acceptHeader, "application/json,application/zip");
    var response = await request.close();

    if (response.statusCode != 200) {
      final contents = StringBuffer();
      await for (var data in response.transform(utf8.decoder)) {
        contents.write(data);
      }
      var body = contents.toString();
      var json = jsonDecode(body);
      throw BackendError.validation(json['message'] ?? 'Server Error: $body');
    }

    final f = await file.open(mode: FileMode.write);
    final keys = response.headers.value('X-JR-DIGILIB-Keys') ?? '[]';
    final endPinjam = response.headers.value('X-JR-DIGILIB-EndPinjam') ?? '1900-01-01';
    // TODO: cancel download kalau back
    s = response.listen((data) {
      f.writeFromSync(data, 0, data.length);
      setState(() {
        progress += data.length * 1.0 / response.contentLength;
      });
    });

    await s?.asFuture();
    await s?.cancel();
    await f.close();

    List<dynamic> bookKeys = jsonDecode(keys);

    Navigator.of(context).pop(BookInfo(DateTime.parse(endPinjam), bookKeys.map((e) => e.toString()).toList()));
  }

  var cancelling = false;
  Future<void>? future;

  @override
  Widget build(BuildContext context) {
    if (future == null) {
      future = download(context);
    }
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) =>
        AlertDialog(
          content: snapshot.hasError
            ? Text('${snapshot.error}')
            : snapshot.hasData
            ? Text('Download selesai')
            : Column(
              mainAxisSize: MainAxisSize.min,
              children: [
              Text('Mendownload File: ${progress >= 1 ? "100" : (progress * 100.0).toStringAsPrecision(2)}%'),
              LinearProgressIndicator(
                value: progress,
                semanticsLabel: 'Mendownload file',
                semanticsValue: '${progress * 100.0} persen',
              )
            ]),
          actions: (snapshot.hasError) ? [
            TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
          ] : [
            cancelling ? TextButton(child: CircularProgressIndicator(), onPressed: null)
            : TextButton(child:Text('Cancel'), onPressed: () async {
              setState(()=> cancelling = true);
              await s?.cancel();
              Navigator.of(context).pop();
            },)
          ],
        )
    );
  }

}

class BookInfo {
  final DateTime returnDate;
  final List<String> bookKeys;

  BookInfo(this.returnDate, this.bookKeys);

  BookInfo.fromJson(Map<String, dynamic> json)
  : returnDate = DateTime.parse(json['returnDate']),
    bookKeys = _parseKeys(json['bookKeys']);
  
  static List<String> _parseKeys(List<dynamic> keys) {
    var l = <String>[];
    for(var key in keys)
      l.add(key.toString());
    return l;
  }
  
  Map<String, dynamic> toJson() => {
    'returnDate': returnDate.toIso8601String().substring(0,10),
    'bookKeys': bookKeys
  };
}