
import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;

class NotificationData {
  DateTime time;
  String text;

  NotificationData({required this.time, required this.text});

  NotificationData.fromJson(Map<String, dynamic> json)
    : time = DateTime.parse(json['time']),
      text = json['text'];
  
  Map<String, dynamic> toJson() => {
    'time': time.toIso8601String(),
    'text': text,
  };
}


class NotificationStore {
  File? _store;
  final List<NotificationData> _data = [
  ];

  static var _instance = NotificationStore._();

  NotificationStore._ ();

  factory NotificationStore() {
    return _instance;
  }

  Future<List<NotificationData>> getData() async {
    await _initStoreFile();
    return _data;
  }

  clear() async {
    await _initStoreFile();
    _data.clear();
    await _store!.delete();
  }

  add(NotificationData notification) async {
    await _initStoreFile();
    _data.insert(0, notification);
    await _store!.writeAsString(jsonEncode(_data.map((e) => e.toJson()).toList()));
  }

  _initStoreFile() async {
    if (_store != null) return;

    var dir = await getApplicationSupportDirectory();
    _store = File(path.join(dir.path, 'notifications.store'));

    if (await _store!.exists()) {
      var contents = await _store!.readAsString();
      List<dynamic> obj = jsonDecode(contents);
      _data.addAll(obj.map((e) => NotificationData.fromJson(e)));
    }
  }
}
