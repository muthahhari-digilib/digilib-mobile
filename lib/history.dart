
import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

class History {
  final File file;
  List<String>? _searchHistory;

  History(this.file);

  Future<List<String>> getHistory() async {
    if (_searchHistory == null) if (await file.exists())
      _searchHistory = await file.readAsLines();
    else
      _searchHistory = [];
    return _searchHistory!;
  }

  Future<void> addHistory(String str) async {
    if (_searchHistory == null) await getHistory();
    var contains = _searchHistory?.contains(str) ?? false;
    _searchHistory!.removeWhere((e) => e == str);
    _searchHistory!.add(str);
    if (contains || _searchHistory!.length > 1000) {
      if (_searchHistory!.length > 1000)
        _searchHistory!.removeRange(0, 100);
      var out = file.openWrite(mode: FileMode.writeOnly);
      _searchHistory!.forEach(out.writeln);
      await out.flush();
      await out.close();
    } else {
      var out = file.openWrite(mode: FileMode.writeOnlyAppend);
      out.writeln(str);
      await out.flush();
      await out.close();
    }
  }
}

class SearchHistory extends History {
  SearchHistory(File file) : super(file);

  Future<List<String>> getSearchHistory() => getHistory();
  Future<void> addSearchHistory(String query) => addHistory(query);

  static Future<SearchHistory> standard() async {
    var dir = await getApplicationDocumentsDirectory();
    await dir.create();
    return SearchHistory(File(path.join(dir.path, '.history')));
  }
}

class BrowseHistory extends History {
  BrowseHistory(File file) : super(file);

  @override
  Future<void> addHistory(String path) => super.addHistory(path);

  static Future<BrowseHistory> standard() async {
    var dir = await getApplicationDocumentsDirectory();
    await dir.create();
    return BrowseHistory(File(path.join(dir.path, '.browse')));
  }
}

