
import 'dart:async';

import 'package:digilib/backend_conn.dart';
import 'package:digilib/config_store.dart';
import 'package:digilib/main_page.dart';
import 'package:digilib/registration_page.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:url_launcher/url_launcher.dart';

import 'main.dart';

class LoginPage extends StatefulWidget {
  final BackendConnection backendConnection;

  const LoginPage({Key? key, required this.backendConnection}) : super(key: key);

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  PhoneNumber? mobile;
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);

    if (mq.orientation == Orientation.landscape)
      return buildLandscape(context);
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        shadowColor: Colors.transparent,
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: DigilibColors.secondary),
      ),
      resizeToAvoidBottomInset: true,
      body: Column(children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(36.0, 96.0, 36.0, 0.0),
                  child: Hero(tag: 'logo', child:Image.asset('images/logo.png')),
                )
              ],
            ),
          ),
        ]),
      bottomSheet: Container(
        padding: EdgeInsets.fromLTRB(24.0, 14.0, 24.0, 24.0),
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Text(
            'Silakan masukkan nomor Mobile Phone Anda:',
          ),
          SizedBox(height: 14),
          Form(
            key: formKey,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey.shade300,
                borderRadius: BorderRadius.circular(24)
              ),
              padding: EdgeInsets.symmetric(horizontal: 19),
              child: InternationalPhoneNumberInput(
                selectorConfig: SelectorConfig(
                  selectorType: PhoneInputSelectorType.DIALOG,
                  setSelectorButtonAsPrefixIcon: true,
                ),
                initialValue: PhoneNumber(dialCode: '+62', isoCode: 'ID', phoneNumber: '000'),
                onInputChanged: (value) => mobile = value,
                inputBorder: InputBorder.none,
              ))
          ),
          SizedBox(height: 24),
          SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(DigilibColors.primary),
                    padding: MaterialStateProperty.all(EdgeInsets.all(13)),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0),
                    ))),
                child: Text(
                  'Masuk',
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: () => sendOTP(context),
              )),
          SizedBox(height: 24),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(text: 'Dengan mengklik \'Masuk\', anda telah menyetujui '),
                TextSpan(
                  text: 'Aturan Penggunaan dan Kebijakan Privasi Jalan Rahmat',
                  style: TextStyle(color: Colors.blue),
                  recognizer: TapGestureRecognizer()..onTap = () async {
                    final url = 'https://jr-digilib.my.id/ketentuan-penggunaan-dan-kebijakan-privasi/';
                    final launchable = await canLaunch(url);
                    if (launchable) {
                      await launch(url);
                    } else {
                      showDialog(context: context, builder: (context) =>
                        AlertDialog(
                          content: Text('Tidak dapat membuka URL. Sila buka https://jr-digilib.my.id dan mengklik Ketentuan Penggunaan dan Kebijakan Privasi'),
                          actions: [
                            TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
                          ],
                        )
                      );
                    }
                  }
                ),
                TextSpan(text:
                ' ("JR"). ' +
                'Anda menyetujui untuk menerima Telepon/SMS/Pesan Singkat dari JR untuk memberikan informasi atau tujuan pemasaran, sesuai ' +
                'dengan aktivitas Anda. Anda dapat menghentikannya dengan berhenti menjadi Anggota.'),
              ],
              style: TextStyle(color: Colors.black)
            ),
            textScaleFactor: 0.7,
          )
        ]),
      ),
    );
  }

  Widget buildLandscape(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        shadowColor: Colors.transparent,
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: DigilibColors.secondary),
      ),
      resizeToAvoidBottomInset: true,
      body: Row(children: <Widget>[
          Column(children: [
            Container(
              width: 300,
              padding: EdgeInsets.fromLTRB(36.0, 48.0, 12.0, 0.0),
              child: Hero(tag: 'logo', child:Image.asset('images/logo.png')),
            ),
            Spacer()
          ]),
      Spacer(),
      Container(
        width: 280,
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 14.0, 14.0),
        decoration: BoxDecoration(color: Colors.white.withAlpha(214)),
        child: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
          Spacer(),
          Text(
            'Silakan masukkan nomor Mobile Phone Anda:',
          ),
          SizedBox(height: 12),
          Form(
            key: formKey,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey.shade300,
                borderRadius: BorderRadius.circular(24)
              ),
              padding: EdgeInsets.symmetric(horizontal: 19),
              child: InternationalPhoneNumberInput(
                selectorConfig: SelectorConfig(
                  selectorType: PhoneInputSelectorType.DIALOG,
                  setSelectorButtonAsPrefixIcon: true,
                ),
                initialValue: PhoneNumber(dialCode: '+62', isoCode: 'ID', phoneNumber: '000'),
                onInputChanged: (value) => mobile = value,
                inputBorder: InputBorder.none,
              )
            ),
          ),
          SizedBox(height: 12),
          SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(DigilibColors.primary),
                    padding: MaterialStateProperty.all(EdgeInsets.all(13)),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0),
                    ))),
                child: Text(
                  'Masuk',
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: () => sendOTP(context),
              )),
          SizedBox(height: 36),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(text: 'Dengan mengklik \'Masuk\', anda telah menyetujui '),
                TextSpan(
                  text: 'Aturan Penggunaan dan Kebijakan Privasi Jalan Rahmat',
                  style: TextStyle(color: Colors.blue),
                  recognizer: TapGestureRecognizer()..onTap = () async {
                    final url = 'https://jr-digilib.my.id/ketentuan-penggunaan-dan-kebijakan-privasi/';
                    final launchable = await canLaunch(url);
                    if (launchable) {
                      await launch(url);
                    } else {
                      showDialog(context: context, builder: (context) =>
                        AlertDialog(
                          content: Text('Tidak dapat membuka URL. Sila buka https://jr-digilib.my.id dan mengklik Ketentuan Penggunaan dan Kebijakan Privasi'),
                          actions: [
                            TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
                          ],
                        )
                      );
                    }
                  }
                ),
                TextSpan(text:
                ' ("JR"). ' +
                'Anda menyetujui untuk menerima Telepon/SMS/Pesan Singkat dari JR untuk memberikan informasi atau tujuan pemasaran, sesuai ' +
                'dengan aktivitas Anda. Anda dapat menghentikannya dengan berhenti menjadi Anggota.'),
              ],
              style: TextStyle(color: Colors.black)
            ),
            textScaleFactor: 0.7,
          )
        ]),
      ),
      ])
    );
  }

  void sendOTP(BuildContext context) async {
    if (formKey.currentState?.validate() != true) {
      return;
    }
    showLoading(context);
    try {
      final res = await widget.backendConnection.checkMobilePhoneAndRegistration(mobile?.phoneNumber ?? '');
      hideLoading(context);

      if (res) {
        doGetNewPermanentSession(context);
      } else {
        doRegistration(context);
      }
    } on BackendError catch (e) {
      hideLoading(context);
      showDialog(context: context, builder: (context) => AlertDialog(
        content: Text('${e.type}: ${e.cause}\n\n${mobile?.phoneNumber}'),
        actions: [
          TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
        ],
      ));
    }
  }

  Future<void> doGetNewPermanentSession(BuildContext context) async {
    var session = await Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => OTPPage(
        backendConnection: widget.backendConnection,
        msisdn: mobile?.phoneNumber ?? '',
        text:Text(''))));

    if (session == null) {
      // TODO: salah OTP
      return;
    }

    // nulis settingnya tidak usah ditunggu
    ConfigStore().setString('session', session);
    widget.backendConnection.sessionID = session;

    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => MainPage(
        backendConnection: widget.backendConnection
      ))
    );
  }
  void doRegistration(BuildContext context) async {
    await Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => OTPPage(
        backendConnection: widget.backendConnection,
        msisdn: mobile!.phoneNumber!,
        text:Text('Anda belum terdaftar.'))));
    
    await Navigator.of(context).push(MaterialPageRoute(builder: (context) => RegistrationPage(
      backendConection: widget.backendConnection,
      msisdn: mobile?.phoneNumber ?? ''
    )));

    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => MainPage(
        backendConnection: widget.backendConnection
      ))
    );
  }
}

class OTPPage extends StatefulWidget {
  final BackendConnection backendConnection;
  final Widget text;
  final String msisdn;

  const OTPPage({Key? key, required this.backendConnection, required this.text, required this.msisdn}) : super(key: key);

  @override
  State<StatefulWidget> createState() => OTPPageState();
}

class OTPPageState extends State<OTPPage> {
  final form = GlobalKey<FormState>();
  final otp = TextEditingController();
  DateTime updateLimit = DateTime.now();
  bool invalidOTP = false;
  String? invalidOTPStr;
  Timer? timer;

  @override
  void initState() {
    super.initState();

    _startTimer(Duration(minutes: 5));
  }

  void _startTimer(Duration duration) {
    updateLimit = DateTime.now().add(duration);
    timer = Timer.periodic(Duration(seconds: 1), (t) {
      setState(() {});
      if (!DateTime.now().isBefore(updateLimit))
        t.cancel();
    });
  }

  String _printDuration(Duration duration) {
    if (duration.inSeconds <= 0) return "00:00";
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  @override
  Widget build(BuildContext context) {
    var delta = updateLimit.difference(DateTime.now());
    var mq = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        title: mq.orientation == Orientation.portrait ? 
                Hero(tag: 'logo', child: Image.asset("images/logo.png")) :
                Text("Masukan OTP"),
        toolbarHeight: mq.orientation == Orientation.portrait ? 200 : 48,
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        foregroundColor: Colors.black,
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: DigilibColors.secondary),
      ),
      body: SingleChildScrollView(child:Padding(
        padding: EdgeInsets.fromLTRB(14, 24, 14, 14),
        child: Column(children: [
          widget.text,
          
          SizedBox(height: mq.orientation == Orientation.portrait ? 20 : 0),
          Text('Silakan masukkan Kode OTP yang telah dikirimkan melalui SMS ke: ' + widget.msisdn,
          textAlign: TextAlign.center,),
          Form(
            key: form,
            child: TextFormField(
            keyboardType: TextInputType.number,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 48),
            controller: otp,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Mohon masukkan OTP dari SMS ke nomor anda';
              }
              if (value.length != 6) {
                return 'Panjang OTP 6 angka, sila periksa kembali';
              }
              if (invalidOTP) {
                invalidOTP = false;
                return invalidOTPStr ?? 'OTP tidak sesuai. Harap periksa kembali';
              }
              return null;
            }
          )),
          SizedBox(height: mq.orientation == Orientation.portrait ? 20 : 14),
          SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(DigilibColors.primary),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ))),
                child: Text(
                  'OK'
                ),
                onPressed: () async {
                  if (form.currentState?.validate() != true) {
                    return;
                  }
                  showLoading(context);
                  var res;
                  try {
                    res = await widget.backendConnection.checkOTP(widget.msisdn, otp.text);
                    hideLoading(context);
                    if (res == "") {
                      invalidOTP = true;
                      form.currentState?.validate();
                    } else {
                      if (timer != null && timer!.isActive)
                        timer!.cancel();
                      Navigator.of(context).pop(res);
                    }
                  } on BackendError catch (e) {
                    hideLoading(context);
                    invalidOTP = true;
                    invalidOTPStr = e.cause;
                    form.currentState?.validate();
                  }
                  
                },
              )),
          SizedBox(height: 14),
          SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.grey),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ))),
                child: Text(
                  'Kirim Ulang Kode OTP (' + _printDuration(delta) + ')'
                ),
                onPressed: delta.compareTo(Duration(seconds: 0)) > 0 ? null : () async {
                  showLoading(context);
                  var res = await widget.backendConnection.resendOTP(widget.msisdn);
                  hideLoading(context);
                  _startTimer(Duration(seconds: res));
                },
              )),
        ],),
      )));
  }
}




void showLoading(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return SimpleDialog(
        children:[Center(child: CircularProgressIndicator()), Center(child:Text('\nLoading...'))]
      );
    }
  );
}

void hideLoading(BuildContext context) {
  Navigator.of(context).pop();
}
