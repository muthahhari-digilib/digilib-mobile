
import 'package:digilib/backend_conn.dart';
import 'package:digilib/config_store.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class RegistrationPage extends StatefulWidget {
  final BackendConnection backendConection;
  final String msisdn;

  const RegistrationPage({Key? key, required this.backendConection, required this.msisdn}) : super(key: key);

  @override
  State<StatefulWidget> createState() => RegistrationPageState();
}
  
class RegistrationPageState extends State<RegistrationPage> {
  var form = GlobalKey<FormState>();
  var reg = RegistrationInfo(name: '', gender: Gender.MALE, birthDate: DateTime.now());
  var birthDate = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(14, 24, 14, 14),
          child: Form(
            key: form,
            child: Column(children: [
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Nama'
                ),
                keyboardType: TextInputType.name,
                onChanged: (value) => reg.name=value,
                validator: (value) => value == null || value.isEmpty ? 'harus diisi' : null,
              ),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'E-Mail'
                ),
                keyboardType: TextInputType.emailAddress,
                onChanged: (value) => reg.email=value,
              ),
              FormField<DateTime?>(
                builder: (ctx) => TextField(
                  decoration: InputDecoration(
                    labelText: 'Tanggal Lahir',
                    suffix: ElevatedButton(
                      child: Text('Pilih'),
                      onPressed: () async {
                        final lastDate = DateTime.now().subtract(Duration(days: 365*5));
                        final picked = await showDatePicker(
                          context: context,
                          initialDate: lastDate,
                          firstDate: DateTime(1921),
                          lastDate: lastDate);
                        if (picked != null && picked != ctx.value) {
                          ctx.didChange(ctx.value);
                          setState(() {
                            reg.birthDate = picked;
                            birthDate.text = DateFormat.yMd().format(picked);
                          });
                        }
                      },
                    )
                  ),
                  readOnly: true,
                  controller: birthDate,
                ),
                validator: (value) => value == null ? 'harus diisi' : null
              ),
              DropdownButtonFormField<Gender>(
                decoration: InputDecoration(
                  labelText: 'Gender'
                ),
                items: [
                  DropdownMenuItem(child: Text('-'), value: null,),
                  DropdownMenuItem(child: Text('Laki-laki'), value: Gender.MALE,),
                  DropdownMenuItem(child: Text('Perempuan'), value: Gender.FEMALE,),
                ],
                value: null,
                onChanged: (gender) => reg.gender = gender,
              ),

              // Tombol kirim registrasi
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.green),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ))),
                  child: Text('Kirim Data Registrasi'),
                  onPressed: () async {
                    try {
                      var firebaseTokenId = await FirebaseMessaging.instance.getToken();
                      var res = await widget.backendConection.register(widget.msisdn, reg, firebaseTokenId ?? '');
                      ConfigStore().setString('session', res);
                      Navigator.of(context).pop(res);
                    } on BackendError catch (e) {
                      showDialog(context: context, builder: (context) =>
                        AlertDialog(
                          content: Text('${e.type}: ${e.cause}'),
                          actions: [
                            TextButton(child:Text('OK'), onPressed: () => Navigator.of(context).pop(),)
                          ],
                        )
                      );
                    }
                  },
                )),
            ],)
          )
        )
      )
    );
  }
}