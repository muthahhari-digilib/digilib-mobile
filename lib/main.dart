import 'dart:io';
import 'dart:math';

import 'package:firebase_core/firebase_core.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;

import 'backend_conn.dart';
import 'backend_conn_impl.dart';
import 'config_store.dart';
import 'main_page.dart';
import 'login.dart';
import 'notification_store.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:package_info_plus/package_info_plus.dart';


Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();

  NotificationStore().add(
    NotificationData(
      time: DateTime.now(),
      text: message.notification?.body ?? "Empty Notification",
    )
  );
}

void main() {
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  runApp(new Digilib());
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: DigilibColors.getPrimarySwatch().shade900
  ));
}

class Tests {
  static const RELOGIN = false;
}

class DigilibColors {
  static const Color primary = Color(0xFFFFB901);
  static const Color primaryAlternative = Color(0xFFFFA213);
  static const Color secondary = Color(0xFF3CD70D);

  // adopted from https://medium.com/@a/using-flutters-primary-swatch-with-a-custom-materialcolor-c5e0f18b95b0
  static MaterialColor getPrimarySwatch() => _getSwatch(primary);
  
  static MaterialColor _getSwatch(Color c) =>
    MaterialColor(c.value, {
      50: _brighten(c, 0.9),
      100: _brighten(c, 0.8),
      200: _brighten(c, 0.6),
      300: _brighten(c, 0.4),
      400: _brighten(c, 0.2),
      500: c,
      600: _brighten(c, -0.2),
      700: _brighten(c, -0.4),
      800: _brighten(c, -0.6),
      900: _brighten(c, -0.9),
    });

  static Color _brighten(Color c, double factor) => Color.fromRGBO(
    _brightenVal(c.red, factor),
    _brightenVal(c.green, factor),
    _brightenVal(c.blue, factor),
    1);
  
  static int _brightenVal(int v, double factor) =>
    _maxmin(factor > 0 ? (v + ((255 - v) * factor)).round()
                       : (v + v * factor).round(), 0, 255);

  static int _maxmin(int v, int minV, int maxV) =>
    max(0, min(v, 255));
}

class Digilib extends StatelessWidget {
  Future<BackendConnection> initConnection() async {
    initFirebaseMessaging();

    // koneksi memanfaatkan versi ini.
    final packageInfo = await PackageInfo.fromPlatform();
    final clientVersion = packageInfo.version + '+' + packageInfo.buildNumber;

    final dir = await getTemporaryDirectory();
    await dir.create();
    final conn = AutoSwitchBackendConnection(BackendConnectionImpl(clientVersion), File(path.join(dir.path, 'contents.cache')));
    if (Tests.RELOGIN) {
      await ConfigStore().setString('session', null);
    }
    final session = await ConfigStore().getString('session');
    conn.sessionID = session;
    return conn;
  }

  ThemeData buildTheme() {
    final base = ThemeData.light();
    final primarySwatch = DigilibColors.getPrimarySwatch();

    return base.copyWith(
      colorScheme: base.colorScheme.copyWith(
        primary: primarySwatch.shade500,
        onPrimary: Colors.white,
        secondary: DigilibColors.secondary,
        onSecondary: Colors.white
      ),
      bottomSheetTheme: BottomSheetThemeData(
        backgroundColor: Colors.white.withAlpha(190), // dipakai di....
      )
    );
  }
  
  Future<void> initFirebaseMessaging() async {
    await Firebase.initializeApp();
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    await messaging.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print(message);
      NotificationStore().add(
        NotificationData(
          time: DateTime.now(),
          text: message.notification?.body ?? "Empty Notification",
        )
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: 
      FutureBuilder<BackendConnection>(
        future: initConnection(),
        builder: (context, session) {
          if (session.connectionState == ConnectionState.active || session.connectionState == ConnectionState.waiting) {
            return LoadingPage();
          } else if (session.hasError) {
            return Scaffold(body: Text('Unexpected Error: ${session.error}'));
          } else if (session.data?.sessionID != null) {
            return MainPage(backendConnection: session.data!);
          } else {
            return LoginPage(backendConnection: session.data!);
          }
        }),
      theme: buildTheme(),
    );
  }
}

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Image.asset('images/background.jpg',
            fit: BoxFit.cover, width: double.infinity, height: double.infinity),
        Column(children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(36.0, 50.0, 36.0, 0.0),
              child: Hero(tag: 'logo', child:Image.asset('images/logo.png')),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 36.0),
              child: CircularProgressIndicator()
            )
        ])
      ]),
    );
  }
}
