
import 'package:cross_local_storage/cross_local_storage.dart';

class ConfigStore {
  var _prefs = LocalStorage.getInstance();
  static var _instance = ConfigStore._();

  ConfigStore._ ();

  factory ConfigStore() {
    return _instance;
  }

  Future<int?> getInt(String key) async {
    return (await _prefs).getInt(key);
  }
  Future<void> setInt(String key, int? value) async {
    if (value == null)
      (await _prefs).remove(key);
    else
      (await _prefs).setInt(key, value);
  }
  Future<String?> getString(String key) async {
    return (await _prefs).getString(key);
  }
  Future<void> setString(String key, String? value) async {
    if (value == null)
      (await _prefs).remove(key);
    else
      (await _prefs).setString(key, value);
  }
}